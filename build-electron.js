const packager = require('electron-packager');
const setLanguages = require('electron-packager-languages');

const INDICATOR_WIN = 'win';
const INDICATOR_LIN = 'lin';

const VERSION = '1.0.0.0';

const defaultOptions = {
  name: 'nlfr-viewer',
  buildPath: __dirname,
  version: VERSION,
  buildVersion: VERSION,
  dir: 'dist',
  out: 'release-builds',
  overwrite: true,
  // asar: true,
  icon: 'dist/assets/logo.ico',
  appCopyright: "Cookiesoft - Lui's Studio",
  prune: true,
  afterCopy: [setLanguages(['en'])],
};

if (process.argv.find((arg) => arg === INDICATOR_WIN))
  packager({
    ...defaultOptions,
    platform: 'win32',
    arch: 'ia32',
    appVersion: VERSION,
    win32metadata: {
      ProductName: 'Easier LF2 Replay Viewer',
    },
  });

if (process.argv.find((arg) => arg === INDICATOR_LIN))
  packager({
    ...defaultOptions,
    platform: 'linux',
    arch: 'x64',
    appVersion: VERSION,
  });
