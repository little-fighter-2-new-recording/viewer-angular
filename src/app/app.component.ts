import { inflate } from 'pako';
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxDropzoneChangeEvent, NgxDropzoneComponent } from 'ngx-dropzone';
import { RendererService } from './modules/player/renderer/renderer.service';
import { IpcService } from './core/services/ipc.service';
import { getExtension } from './shared/utilities/file.utility';
import { RecordingService } from './core/services/recording.service';
import BinaryParser from './core-lib/parsers/binary-parser';
import { RenderControlsService } from './modules/player/renderer/render-controls.service';
import { RoomService } from './core/services/room.service';
import { environment } from '../environments/environment';
import { FileSelectorService } from './core/services/file-selector.service';
import { ROUTE_ROOM_CREATE } from './routes';
import { WsService } from './core/services/ws.service';
import {
  SUPPORTED_ARCHIVE_EXTENSIONS,
  SUPPORTED_FILE_EXTENSIONS,
} from './shared/constants/app-constants';

@Component({
  selector: 'nlfr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @ViewChild('drop')
  public dropElement: NgxDropzoneComponent | undefined;

  public readonly SUPPORTED_FILE_EXTENSIONS = SUPPORTED_FILE_EXTENSIONS.map(
    (e) => `.${e}`,
  ).join(',');

  constructor(
    private readonly _ipcService: IpcService,
    private readonly _rendererService: RendererService,
    private readonly _recordingService: RecordingService,
    private readonly _renderControlsService: RenderControlsService,
    private readonly _roomService: RoomService,
    private readonly _wsService: WsService,
    private readonly _fileSelectorService: FileSelectorService,
    private readonly _router: Router,
  ) {
    this._fileSelectorService.openFileSelector.subscribe(() => {
      this.dropElement?.showFileSelector();
    });
  }

  private static async getData(file: File): Promise<Uint8Array> {
    if (
      SUPPORTED_ARCHIVE_EXTENSIONS.includes(
        getExtension(file.name).toLowerCase(),
      )
    ) {
      const data = new Uint8Array(await file.arrayBuffer());
      return inflate(data);
    }

    return new Uint8Array(await file.arrayBuffer());
  }

  async onSelect(event: NgxDropzoneChangeEvent): Promise<void> {
    if (!event.addedFiles || event.addedFiles.length === 0) return;

    const possibleFiles = event.addedFiles.filter((f) =>
      SUPPORTED_FILE_EXTENSIONS.includes(getExtension(f.name).toLowerCase()),
    );

    if (this._router.url === `/${ROUTE_ROOM_CREATE}`) {
      this._fileSelectorService.files.next(possibleFiles);
      return;
    }

    if (possibleFiles.length === 0) return;

    this.testUpload(possibleFiles[0]);

    this._recordingService.recording = undefined;
    this._renderControlsService.currentSceneIndex = 0;
    this._recordingService.recording = new BinaryParser(
      await AppComponent.getData(possibleFiles[0]),
    ).parse();
  }

  private async testUpload(recording: File): Promise<void> {
    const data = new FormData();
    data.append('file', recording, recording.name);

    const response = await fetch(`${environment.backendURL}room/recording`, {
      method: 'put',
      headers: {
        Authorization: `Bearer ${this._roomService.wsToken}`,
      },
      body: data,
    });
    console.log(response);

    const response2 = await fetch(`${environment.backendURL}room/recording`, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${this._roomService.wsToken}`,
      },
    });
    console.log(await response2.arrayBuffer());
  }
}
