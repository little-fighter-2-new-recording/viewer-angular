import { Component } from '@angular/core';
import { DebugService } from '../../services/debug.service';

@Component({
  selector: 'nlfr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(private readonly _debugService: DebugService) {}

  changeDebugCamera(): void {
    this._debugService.threeDCamera = !this._debugService.threeDCamera;
  }
}
