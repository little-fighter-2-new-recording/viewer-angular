import { Component } from '@angular/core';

@Component({
  selector: 'nlfr-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  public time = new Date();

  constructor() {
    //
  }
}
