import { NgModule } from '@angular/core';

/**
 *
 * @param parentModule
 * @param moduleName
 * @source https://github.com/mathisGarberg/angular-folder-structure/blob/master/src/app/core/guard/module-import.guard.ts
 */
export function throwIfAlreadyLoaded(
  parentModule: NgModule,
  moduleName: string,
) {
  if (parentModule)
    throw new Error(
      `${moduleName} has already been loaded. Import ${moduleName} modules in the AppModule only.`,
    );
}
