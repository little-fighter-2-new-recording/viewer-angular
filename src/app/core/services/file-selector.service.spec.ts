import { TestBed } from '@angular/core/testing';

import { FileSelectorService } from './file-selector.service';

describe('FileSelectorService', () => {
  let service: FileSelectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileSelectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
