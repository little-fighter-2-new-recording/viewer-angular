import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileSelectorService {
  public readonly openFileSelector = new Subject<never>();

  public readonly files = new BehaviorSubject<Blob[]>([]);

  // public constructor() {}
}
