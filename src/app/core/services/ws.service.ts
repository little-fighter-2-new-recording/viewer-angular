import { Injectable, OnDestroy } from '@angular/core';
import { RenderControlsService } from '../../modules/player/renderer/render-controls.service';
import { RendererService } from '../../modules/player/renderer/renderer.service';
import { RoomService } from './room.service';
import { SubscribeEvents } from '../../shared/classes/subscribe-events';
import {
  EVENTS_ROOM_FPS_CHANGED,
  EVENTS_ROOM_PLAY_STATE_CHANGED,
  MESSAGE_ROOM_GET_OPTIONS,
  MESSAGE_ROOM_SET_FPS,
  MESSAGE_ROOM_SET_OPTIONS,
  MESSAGE_ROOM_SET_PLAY_STATE,
} from '../../core-lib/events.constants';
import RoomOptionsDTO from '../../core-lib/dtos/room-options.dto';
import RoomPlayStateDTO from '../../core-lib/dtos/room-play-state.dto';

@Injectable({
  providedIn: 'root',
})
export class WsService extends SubscribeEvents implements OnDestroy {
  private _blockChangesFromControls = false;

  public constructor(
    private readonly _controlsService: RenderControlsService,
    private readonly _rendererService: RendererService,
    private readonly _roomService: RoomService,
  ) {
    super();

    this.subscribes.push(
      this._rendererService.sceneCreated.subscribe(() => {
        if (!this._roomService.isConnected || !this._roomService.socket) return;

        if (this._roomService.isRoomOwner) {
          this._roomService.socket.emit(
            MESSAGE_ROOM_SET_OPTIONS,
            new RoomOptionsDTO({
              fps: this._controlsService.fps,
              isPause: this._controlsService.pause,
              currentIndex: this._controlsService.currentSceneIndex,
            }),
          );
        } else {
          this._roomService.socket.emit(
            MESSAGE_ROOM_GET_OPTIONS,
            (data: RoomOptionsDTO) => {
              this.sendControls(() => {
                this._controlsService.fps = data.fps;
                this._controlsService.pause = data.isPause;
                this._controlsService.currentSceneIndex = data.currentIndex;
              });
            },
          );
        }
      }),
      this._roomService.connectedEvent.subscribe((isConnected) => {
        if (!isConnected || !this._roomService.socket) return;

        this._roomService.socket.on(
          EVENTS_ROOM_PLAY_STATE_CHANGED,
          (data: RoomPlayStateDTO) => {
            // TODO: clever alg -> if diff < 2sec then play until currentIndex
            this.sendControls(() => {
              this._controlsService.pause = data.isPause;
              this._controlsService.currentSceneIndex = data.currentIndex;
            });
          },
        );

        this._roomService.socket.on(EVENTS_ROOM_FPS_CHANGED, (fps: number) => {
          this.sendControls(() => {
            this._controlsService.fps = fps;
          });
        });
      }),
      this._controlsService.pauseEvent.subscribe((isPause) => {
        if (
          this._blockChangesFromControls ||
          !this._roomService.socket ||
          !this._roomService.isConnected
        )
          return;

        this._roomService.socket.emit(
          MESSAGE_ROOM_SET_PLAY_STATE,
          new RoomPlayStateDTO({
            isPause: isPause,
            currentIndex: this._controlsService.currentSceneIndex,
          }),
        );
      }),
      this._controlsService.currentSceneIndexEvent.subscribe((index) => {
        if (
          this._blockChangesFromControls ||
          !this._roomService.socket ||
          !this._roomService.isConnected ||
          !this._controlsService.pause
        )
          return;

        this._roomService.socket.emit(
          MESSAGE_ROOM_SET_PLAY_STATE,
          new RoomPlayStateDTO({
            isPause: this._controlsService.pause,
            currentIndex: index,
          }),
        );
      }),
      this._controlsService.fpsChanged.subscribe((fps) => {
        if (
          this._blockChangesFromControls ||
          !this._roomService.socket ||
          !this._roomService.isConnected
        )
          return;

        this._roomService.socket.emit(MESSAGE_ROOM_SET_FPS, fps);
      }),
    );
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private sendControls(func: () => void): void {
    this._blockChangesFromControls = true;
    try {
      func();
    } catch (e) {
      console.error(e);
    }
    this._blockChangesFromControls = false;
  }
}
