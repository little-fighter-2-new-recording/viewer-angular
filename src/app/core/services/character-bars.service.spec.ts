import { TestBed } from '@angular/core/testing';

import { CharacterBarsService } from './character-bars.service';

describe('CharacterBarsService', () => {
  let service: CharacterBarsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CharacterBarsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
