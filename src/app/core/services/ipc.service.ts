import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcRenderer } from 'electron';
import {
  EVENT_GET_RECORDING_BINARY,
  EVENT_GET_RECORDING_RESULT_BINARY,
} from '../../../../electron/events.constants';
import { RecordingService } from './recording.service';
import BinaryParser from '../../core-lib/parsers/binary-parser';
import { RoomService } from './room.service';

@Injectable({
  providedIn: 'root',
})
export class IpcService {
  private _ipc: IpcRenderer | undefined;

  public constructor(
    private readonly _recordingService: RecordingService,
    private readonly router: Router,
    private readonly nz: NgZone,
    private readonly _wsService: RoomService, // just debug
  ) {
    if ((window as any).require) {
      try {
        this._ipc = (window as any).require('electron').ipcRenderer;

        if (!this._ipc) throw new Error();

        this._ipc.on(
          EVENT_GET_RECORDING_RESULT_BINARY,
          (event, recording: Uint8Array) => {
            this.nz.run(() => {
              this._wsService.testUpload(recording);
              const parser = new BinaryParser(recording);
              this._recordingService.recording = parser.parse();
            });
          },
        );

        this._ipc.send(
          EVENT_GET_RECORDING_BINARY,
          '../../../20210424_152354.nlfr',
        );
      } catch (error) {
        throw error;
      }
    } else {
      console.error('Could not load electron ipc');
      // throw new Error('Could not load electron ipc');
    }
  }
}
