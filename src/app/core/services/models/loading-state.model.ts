export type LoadingState = {
  file: string;
  index: number;
  total: number;
};
