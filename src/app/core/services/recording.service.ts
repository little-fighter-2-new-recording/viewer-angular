import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RecordingFormat } from '../../core-lib/models/recording-format.model';
import DataListFrame from '../../core-lib/models/data-list-frame.model';
import { Size } from '../../shared/models/size.model';

@Injectable({
  providedIn: 'root',
})
export class RecordingService {
  private _recording$$ = new BehaviorSubject<RecordingFormat | undefined>(
    undefined,
  );

  public readonly recording$ = this._recording$$.asObservable();

  public get recording(): RecordingFormat | undefined {
    return this._recording$$.value;
  }

  public set recording(record: RecordingFormat | undefined) {
    if (record === this._recording$$.value) return;

    this._recording$$.next(record);
  }

  public getFrame(dataID: number, frameID: number): DataListFrame | undefined {
    const recording = this._recording$$.value;
    if (!recording) return undefined;

    const data = recording.dataList.find((d) => d.dataID === dataID);
    if (!data) return undefined;

    return data.frames.find((f) => f.frameID === frameID);
  }

  public getSpritesheetSize(dataID: number, picIndex: number): Size {
    const recording = this._recording$$.value;
    if (!recording) return { width: 0, height: 0 };

    const data = recording.dataList.find((d) => d.dataID === dataID);
    if (!data) return { width: 0, height: 0 };

    let picIndexStart = 0;
    for (const sheet of data.spritesheets) {
      if (sheet.overridePicStartIndex)
        picIndexStart = sheet.overridePicStartIndex;

      if (
        picIndexStart >= picIndex &&
        picIndex <= picIndexStart + sheet.col * sheet.row - 1
      )
        return {
          width: sheet.width,
          height: sheet.height,
        };

      picIndexStart += sheet.col * sheet.row;
    }

    return { width: 0, height: 0 };
  }
}
