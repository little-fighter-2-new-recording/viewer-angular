import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import openSocket from 'socket.io-client';
import { environment } from '../../../environments/environment';
import { fetchRequest } from '../../shared/utilities/fetcher.utility';
import { EHttpStatusCode } from '../../shared/models/http-status-code.enum';
import { EErrorCode } from '../../core-lib/error-code.enum';
import RoomCreatedDTO from '../../core-lib/dtos/room-created.dto';
import RoomCreatorDTO from '../../core-lib/dtos/room-creator.dto';
import RoomJoinDTO from '../../core-lib/dtos/room-join.dto';
import RoomJoinedDTO from '../../core-lib/dtos/room-joined.dto';
import {
  LOCAL_STORAGE_HTTP_UUID,
  LOCAL_STORAGE_ROOM_ID,
  LOCAL_STORAGE_WEB_SOCKET_TOKEN,
} from '../../shared/constants/local-storage.contants';
import {
  EVENTS_ROOM_PARTICIPANTS_CHANGED,
  EVENTS_ROOM_RECORDING_CHANGED,
  MESSAGE_ROOM_HAS_RECORDING,
} from '../../core-lib/events.constants';
import { RecordingService } from './recording.service';
import BinaryParser from '../../core-lib/parsers/binary-parser';
import { SubscribeEvents } from '../../shared/classes/subscribe-events';
import { Socket } from 'socket.io';
import { EPermission } from '../../core-lib/dtos/permission.enum';

type Participant = {
  username: string;
  httpID: string;

  socket: Socket | undefined;
  permission?: EPermission;
};

@Injectable({
  providedIn: 'root',
})
export class RoomService extends SubscribeEvents implements OnDestroy {
  public socket?: SocketIOClient.Socket;

  private _isConnectedSub = new BehaviorSubject<boolean>(false);

  public connectedEvent = this._isConnectedSub.asObservable();

  private _recording: Uint8Array | undefined;
  private _wsToken: string | undefined;
  private _httpID: string | undefined;
  private _roomID: string | undefined;
  private _room: RoomJoinedDTO | undefined;
  private _isRoomOwner = false;

  public get isConnected(): boolean {
    return this._isConnectedSub.value && !!this.socket;
  }

  public get socketClientID(): string {
    return this.socket?.id ?? '';
  }

  public get roomID(): string | undefined {
    return this._roomID;
  }

  public set roomID(val: string | undefined) {
    this._roomID = val;

    if (val) localStorage.setItem(LOCAL_STORAGE_ROOM_ID, val);
    else localStorage.removeItem(LOCAL_STORAGE_ROOM_ID);
  }

  public get wsToken(): string | undefined {
    return this._wsToken;
  }

  public set wsToken(val: string | undefined) {
    this._wsToken = val;

    if (val) localStorage.setItem(LOCAL_STORAGE_WEB_SOCKET_TOKEN, val);
    else localStorage.removeItem(LOCAL_STORAGE_WEB_SOCKET_TOKEN);
  }

  public get httpID(): string | undefined {
    return this._httpID;
  }

  public set httpID(val: string | undefined) {
    this._httpID = val;

    if (val) localStorage.setItem(LOCAL_STORAGE_HTTP_UUID, val);
    else localStorage.removeItem(LOCAL_STORAGE_HTTP_UUID);
  }

  public get isRoomOwner(): boolean {
    return this._isRoomOwner;
  }

  public constructor(private readonly _recordingService: RecordingService) {
    super();
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  public async testUpload(recording: Uint8Array): Promise<boolean> {
    const data = new FormData();
    data.append('file', new Blob([recording]), 'test.nlfr');

    const response = await fetch(`${environment.backendURL}room/recording`, {
      method: 'put',
      headers: {
        Authorization: `Bearer ${this._wsToken}`,
      },
      body: data,
    });

    console.log(response);
    return response.status === EHttpStatusCode.OK;
  }

  public async createRoom(data: RoomCreatorDTO): Promise<boolean> {
    const response = await fetchRequest('room/create', 'post', undefined, data);

    if (response.status === EHttpStatusCode.CREATED) {
      const result = (await response.json()) as RoomCreatedDTO;
      this.wsToken = result.wsToken;
      this.httpID = result.id;
      this._roomID = result.roomID;
      this._isRoomOwner = true;
      console.log(result);

      return await this.connectToWebSocketServer();
      //       if (this._recording) this.testUpload(this._recording);
    }

    return false;
  }

  public async joinRoom(data: RoomJoinDTO): Promise<boolean> {
    const response = await fetchRequest('room/join', 'put', undefined, data);

    if (response.status === EHttpStatusCode.ACCEPTED) {
      const result = (await response.json()) as RoomJoinedDTO;
      this.wsToken = result.wsToken;
      this.httpID = result.id;
      this._isRoomOwner = false;

      return await this.connectToWebSocketServer();
      //       if (this._recording) this.testUpload(this._recording);
    }

    return false;
  }

  public async getAndSetRecordingAndRecordingSettings(): Promise<void> {
    const response = await fetchRequest('room/recording', 'get', this._wsToken);
    if (response.status === EHttpStatusCode.OK) {
      const recording = await response.arrayBuffer();
      if (recording.byteLength === 0)
        this._recordingService.recording = undefined;
      else
        this._recordingService.recording = new BinaryParser(
          new Uint8Array(recording),
        ).parse();
    }
  }

  public async hasRecordingFile(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      if (!this.socket) {
        resolve(false);
        return;
      }

      this.socket.emit(
        MESSAGE_ROOM_HAS_RECORDING,
        (hasRecordingFile: boolean) => {
          resolve(hasRecordingFile);
        },
      );
    });
  }

  private async connectToWebSocketServer(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      if (!this._wsToken) {
        resolve(false);
        return;
      }

      this.socket = openSocket(environment.backendURL, {
        query: {
          tmpToken: this._wsToken,
        },
      });

      if (!this.socket) {
        resolve(false);
        return;
      }

      this.socket.on('connect', () => {
        // magic shit so that the server can "abort", coz adapter doesnt work...
        setTimeout(() => {
          this._isConnectedSub.next(true);
          resolve(true);
        }, 200);
      });

      this.socket.on('disconnect', () => {
        resolve(false);
        this.connectionClosed();
      });

      this.socket.on('error', (data: EErrorCode) => {
        if (data === EErrorCode.roomDoesNotExit) {
          this.wsToken = undefined; // remove token, because token is invalid
          resolve(false);
          this.connectionClosed();
        }
        console.error('ERROR!!!', data, data === EErrorCode.roomDoesNotExit);
      });

      this.socket.on(EVENTS_ROOM_RECORDING_CHANGED, async () => {
        await this.getAndSetRecordingAndRecordingSettings();
      });

      this.socket.on(
        EVENTS_ROOM_PARTICIPANTS_CHANGED,
        (participants: Participant[]) => {
          console.log(participants);
        },
      );
    });
  }

  private connectionClosed(): void {
    this._isConnectedSub.next(false);
  }
}
