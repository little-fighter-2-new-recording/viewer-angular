import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DebugService {
  private readonly _debug = new BehaviorSubject<boolean>(true);
  private readonly _3dCamera = new BehaviorSubject<boolean>(false);

  public readonly debugEvent = this._debug.asObservable();
  public readonly threeDCameraEvent = this._3dCamera.asObservable();

  public get debug(): boolean {
    return this._debug.value;
  }

  public get threeDCamera(): boolean {
    return this._3dCamera.value;
  }

  public set threeDCamera(val: boolean) {
    if (!this._debug.value) return;

    this._3dCamera.next(val);
  }
}
