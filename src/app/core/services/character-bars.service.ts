import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  BAR_BACKGROUND_HEIGHT,
  BAR_BACKGROUND_WIDTH,
  BARS_MAXIMUM,
  BARS_MAXIMUM_PER_COL,
} from '../../shared/constants/little-fighter.constants';
import { Position } from '../../shared/models/position.model';
import { RecordingService } from './recording.service';

@Injectable({
  providedIn: 'root',
})
export class CharacterBarsService {
  private readonly _barPositions$$: BehaviorSubject<Position[]>;
  public readonly barPositions$: Observable<Position[]>;

  public get barPositions(): Position[] {
    return this._barPositions$$.value;
  }

  public constructor(private readonly _recordingService: RecordingService) {
    this._barPositions$$ = new BehaviorSubject<Position[]>(
      CharacterBarsService.getDefaultPosition(),
    );
    this.barPositions$ = this._barPositions$$.asObservable();

    /*
    this._recordingService.recordingEvent.subscribe(() =>
      this.resetPositions(),
    );
     */
  }

  public static getDefaultPosition(): Position[] {
    const bars: Position[] = [];
    let x = 0;
    let y = 0;

    for (let i = 0; i < BARS_MAXIMUM; ++i) {
      bars.push({ x, y });
      x += BAR_BACKGROUND_WIDTH;
      if (x % (BAR_BACKGROUND_WIDTH * BARS_MAXIMUM_PER_COL) == 0) {
        y += BAR_BACKGROUND_HEIGHT;
        x = 0;
      }
    }

    return bars;
  }

  public resetPositions(): void {
    this._barPositions$$.next(CharacterBarsService.getDefaultPosition());
  }

  public changeBarPosition(index: number, pos: Position): void {
    if (index < 0 || index > this._barPositions$$.value.length) {
      console.warn('Bar index out of range!');
      return;
    }

    const bars = this._barPositions$$.value;
    bars[index] = pos;
    this._barPositions$$.next(bars);
  }
}
