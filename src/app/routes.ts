import { Route } from '@angular/router';
import { PlayerComponent } from './modules/player/player.component';
import { LoadingDialogComponent } from './modules/player/loading-dialog/loading-dialog.component';
import { HomeComponent } from './modules/start/home/home.component';
import { RoomCreatorComponent } from './modules/start/room-creator/room-creator.component';
import { RoomJoinerComponent } from './modules/start/room-joiner/room-joiner.component';

export const ROUTE_ROOM_CREATE = 'room/create';

export const ROUTES: Route[] = [
  {
    path: 'loading',
    component: LoadingDialogComponent,
  },
  {
    path: 'viewer',
    component: PlayerComponent,
  },
  {
    path: ROUTE_ROOM_CREATE,
    component: RoomCreatorComponent,
  },
  {
    path: 'room/join/:id',
    component: RoomJoinerComponent,
  },
  {
    path: 'room/join',
    component: RoomJoinerComponent,
  },
  {
    path: '**',
    component: HomeComponent,
  },
];
