import { Position } from './position.model';

export type ThreeDPosition = Position & {
  z: number;
};
