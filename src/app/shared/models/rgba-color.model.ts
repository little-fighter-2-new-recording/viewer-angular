import { RGBColor } from './rgb-color.model';

export type RGBAColor = RGBColor & {
  a: number;
};
