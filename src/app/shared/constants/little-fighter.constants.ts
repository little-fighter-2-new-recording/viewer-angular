export const WINDOW_WIDTH = 794;
export const WINDOW_HEIGHT = 550;
export const MAXIMUM_POSITION_Z = 500;
export const MAXIMUM_POSITION_Z_TOLERANCE = MAXIMUM_POSITION_Z - 10;
export const MAXIMUM_POSITION_Z_DEPTH = WINDOW_HEIGHT * 2; // actually it is lower but in case of mods (NTSD etc.)
export const MAXIMUM_BACKGROUND_LAYERS = 150; // actually it is lower but in case of mods (NTSD etc.)

export const VIEWER_VIEW_PORT_WIDTH = WINDOW_WIDTH;
export const VIEWER_VIEW_PORT_HEIGHT = WINDOW_HEIGHT;

// ------------ Spark ------------
export const SPARK_SMALL_SPRITE_WIDTH = 61;
export const SPARK_SMALL_SPRITE_HEIGHT = 48;
export const SPARK_LARGE_SPRITE_WIDTH = 102;
export const SPARK_LARGE_SPRITE_HEIGHT = 80;

export const SPARK_ID_HIT_SMALL = 5;
export const SPARK_ID_HIT_LARGE = 0;
export const SPARK_ID_BLOOD_SMALL = 15;
export const SPARK_ID_BLOOD_LARGE = 10;
export const SPARK_MINIMUM_TO_DRAW = 500;
export const SPARK_START_POSITION_Z = -10;

// ------------ Text ------------
export const AUTHOR_X_POSITION = 10;
export const AUTHOR_Y_POSITION = -147;
export const AUTHOR_X_POSITION_CONTENT = AUTHOR_X_POSITION + 65;

export const INFO_X_POSITION = AUTHOR_X_POSITION + 16;
export const INFO_Y_POSITION = AUTHOR_Y_POSITION - 22;
export const INFO_X_POSITION_CONTENT = AUTHOR_X_POSITION_CONTENT;

export const TIME_X_POSITION = 5;
export const TIME_Y_POSITION = -WINDOW_HEIGHT + 26;

export const THREE_D_BORDER_COLOR = '#444444';
export const THREE_D_FORE_COLOR = '#ffffff';

export const GAME_INFO_POSITION_RIGHT_BOTTOM_OFFSET_X = 7;
export const GAME_INFO_POSITION_RIGHT_BOTTOM_OFFSET_Y = 16;
export const GAME_INFOS_POSITION_Y = 0;

export const GAME_INFO_STAGE_POSITION_Y = -124;

// ------------ Bars ------------
export const BAR_BACKGROUND_WIDTH = 198;
export const BAR_BACKGROUND_HEIGHT = 54;

export const BARS_MAXIMUM_PER_COL = 4;
export const BARS_MAXIMUM = 8;
export const BARS_POSITION_Z = MAXIMUM_POSITION_Z_TOLERANCE - BARS_MAXIMUM;

export const BAR_POSITION_X = 57;
export const BAR_POSITION_Y = 16;
export const BAR_POSITION_Y2 = 36;
export const BAR_POSITION_INDEX_Z = 0.2;
export const BAR_POSITION_BACK_INDEX_Z = 0.1;
export const BAR_HEIGHT = 10;
export const BAR_MAX_WIDTH = 124;

export const BAR_COLOR_HP = '#ff0000';
export const BAR_COLOR_HP_DARK = '#6f081f';
export const BAR_COLOR_MP = '#0000ff';
export const BAR_COLOR_MP_DARK = '#1f086f';

export const BAR_SMALL_POSITION_X = 9;
export const BAR_SMALL_POSITION_Y = 7;

export const AI_COM_START = 10;

export const SMALL_SIZE_WIDTH = 40;
export const SMALL_SIZE_HEIGHT = 45;

export const OBJECTS_START_POSITION_Z =
  SPARK_START_POSITION_Z - MAXIMUM_POSITION_Z_DEPTH;
export const BACKGROUND_START_POSITION_Z =
  OBJECTS_START_POSITION_Z - MAXIMUM_BACKGROUND_LAYERS;

export const OBJECTS_SHADOW_POSITION_OFFSET_Z = 0;
export const OBJECTS_SPRITE_POSITION_OFFSET_Z = 0.1;
export const OBJECTS_CHARACTER_BLOOD_POSITION_OFFSET_Z = 0.2;
export const OBJECTS_NAME_POSITION_OFFSET_Z = 0.3;
