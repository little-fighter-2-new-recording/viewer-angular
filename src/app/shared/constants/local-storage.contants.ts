export const LOCAL_STORAGE_TOKEN = 'token';
export const LOCAL_STORAGE_WEB_SOCKET_TOKEN = 'wsToken';
export const LOCAL_STORAGE_ROOM_ID = 'room';
export const LOCAL_STORAGE_HTTP_UUID = 'id';
