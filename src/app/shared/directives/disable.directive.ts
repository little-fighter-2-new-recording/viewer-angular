/**
 * Important!
 * The class has been copied from an article on the subject: https://imdurgeshpal.medium.com/enable-disable-all-dom-elements-inside-div-in-angular-using-directive-56ca4602ab7b
 * @source https://imdurgeshpal.medium.com/enable-disable-all-dom-elements-inside-div-in-angular-using-directive-56ca4602ab7b
 */

import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
} from '@angular/core';

const DISABLED = 'disabled';
const APP_DISABLED = 'app-disabled';
const TAB_INDEX = 'tabindex';
const TAG_ANCHOR = 'a';

@Directive({
  selector: '[appDisable]',
})
export class DisableDirective implements OnChanges, AfterViewInit {
  @Input()
  public appDisable = true;

  public constructor(
    private eleRef: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {}

  public ngOnChanges(): void {
    this.disableElement(this.eleRef.nativeElement);
  }

  public ngAfterViewInit(): void {
    this.disableElement(this.eleRef.nativeElement);
  }

  private disableElement(element: any) {
    const elements = [element];

    while (elements.length > 0) {
      const ele = elements.splice(0, 1)[0];

      if (this.appDisable) {
        if (!ele.hasAttribute(DISABLED)) {
          this.renderer.setAttribute(ele, APP_DISABLED, '');
          this.renderer.setAttribute(ele, DISABLED, 'true');

          // disabling anchor tab keyboard event
          if (ele.tagName.toLowerCase() === TAG_ANCHOR) {
            this.renderer.setAttribute(ele, TAB_INDEX, '-1');
          }
        }
      } else {
        if (ele.hasAttribute(APP_DISABLED)) {
          if (ele.getAttribute('disabled') !== '') {
            ele.removeAttribute(DISABLED);
          }
          ele.removeAttribute(APP_DISABLED);
          if (ele.tagName.toLowerCase() === TAG_ANCHOR) {
            ele.removeAttribute(TAB_INDEX);
          }
        }
      }

      if (ele.children) elements.push(...ele.children);
    }
  }
}
