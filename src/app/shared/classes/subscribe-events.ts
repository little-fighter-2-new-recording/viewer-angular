import { Subscription } from 'rxjs';

export class SubscribeEvents {
  protected readonly subscribes: Subscription[] = [];

  public unsubscribeListeners(): void {
    for (const sub of this.subscribes) {
      sub.unsubscribe();
    }

    this.subscribes.splice(0);
  }
}
