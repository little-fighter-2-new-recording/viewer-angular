/**
 * This function is supposed to calculated the modulo of a given number to a given base.
 * @param val This is the value that is being viewed in the realm of the base.
 * @param base This is the base the given value is being viewed in.
 * @return Returns the value transformed into a value mod the base.
 */
export function mod(val: number, base: number): number {
  // second addition and mod calculation supposed to transform negative values (val < 0) into positive ones (val > 0)
  return ((val % base) + base) % base;
}
