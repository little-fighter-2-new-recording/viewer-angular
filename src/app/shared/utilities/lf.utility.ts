import { EMode } from '../../core-lib/models/mode.enum';
import { EDifficultyMode } from '../../core-lib/models/difficulty-mode.enum';
import SceneObject from '../../core-lib/models/scene-object.model';
import Scene from '../../core-lib/models/scene.model';
import StageScene from '../../core-lib/models/stage-scene.model';

const LITTLE_FIGHTER_STAGE_SURVIVAL = 50;
const MAXIMAL_SUB_STAGES = 10;

export function getTotalTimeFromFPS(totalImages: number, fps = 30): string {
  const totalSecs = Math.floor(totalImages / fps) + 1; // +1 coz 0-based
  const mins = Math.floor(totalSecs / 60);
  const secs = totalSecs - mins * 60;

  return `${mins.toString().padStart(2, '0')}:${secs
    .toString()
    .padStart(2, '0')}`;
}

export function getModeString(mode: number): string {
  if (mode == EMode.VS) return 'VS mode';
  else if (mode == EMode.STAGE) return 'Stage mode';
  else if (mode == EMode.ONE_ON_1) return '1 on 1';
  else if (mode == EMode.TWP_ON_2) return '2 on 2';
  else if (mode == EMode.BATTLE) return 'Battle mode';
  else if (mode == EMode.DEMO) return '';

  return '-';
}

export function getDifficultyString(diff: number): string {
  if (diff == EDifficultyMode.CRAZY) return 'CRAZY!';
  else if (diff == EDifficultyMode.DIFFICULT) return 'Difficult';
  else if (diff == EDifficultyMode.NORMAL) return 'Normal';
  else if (diff == EDifficultyMode.EASY) return 'Easy';

  return '-';
}

export function isObjectVisible(sceneObj: SceneObject): boolean {
  return sceneObj.blink % 4 < 2;
}

export function getStageDescription(scene: Scene): string | undefined {
  if (!(scene instanceof StageScene)) return undefined;

  if (scene.stage === LITTLE_FIGHTER_STAGE_SURVIVAL)
    return `Survival Stage: ${scene.phase}`;

  const stage = Math.floor(scene.stage / MAXIMAL_SUB_STAGES);
  const subStage = scene.stage - stage * MAXIMAL_SUB_STAGES;
  return `Stage ${stage + 1}-${subStage}`; // stage + 1 coz 1-based in lf2
}
