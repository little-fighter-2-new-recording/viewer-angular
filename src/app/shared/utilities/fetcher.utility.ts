import { EHttpStatusCode } from '../models/http-status-code.enum';
import { environment } from '../../../environments/environment';

const DEFAULT_FETCH_TIMEOUT_MS = 1000 * 30;

/**
 * This function serves as a blanket fetch request that allows custom request through it's various parameters.
 * It's most defining feature is the fact that it sets and works with a timeout that allows the cancellation
 * of a started request after a set period of time.
 * @param url This is the url that is going to be fetched to.
 * @param method This is the (optional) method with which the fetch is going to be performed (e.g. get and so on).
 * @param token This is the (optional) authentication token required for a lot of fetch requests in the backend.
 * @param body The (optional) body of the request.
 * @param timeoutMS This is the (optional) given time after which the fetch request is going to be aborted if unable to obtain and answer prior that time mark.
 * @param overrideHeaders
 * @return Returns a response object containing the information returned from the server.
 * @source https://davidwalsh.name/fetch-timeout
 * @see Response
 */
export async function fetchRequest(
  url: string,
  method: 'get' | 'post' | 'put' | 'delete' = 'get',
  token?: string,
  // eslint-disable-next-line @typescript-eslint/ban-types
  body?: {},
  timeoutMS?: number,
  // eslint-disable-next-line @typescript-eslint/ban-types
  overrideHeaders: Headers | {} = {},
): Promise<Response> {
  // set default timeout if no other one is given
  if (!timeoutMS || timeoutMS < 100) timeoutMS = DEFAULT_FETCH_TIMEOUT_MS;

  return new Promise<Response>((resolve, reject) => {
    let didTimeOut = false;
    const timeout = setTimeout(() => {
      didTimeOut = true;
      resolve(
        new Response(
          JSON.stringify({
            statusCode: EHttpStatusCode.GATEWAY_TIMEOUT,
            error: 'Timeout',
            message: 'Timeout.',
          }),
          {
            headers: {},
            status: EHttpStatusCode.GATEWAY_TIMEOUT,
            statusText: 'TIMEOUT',
          } as ResponseInit,
        ),
      );
    }, timeoutMS);

    _fetchRequest(url, method, token, body, overrideHeaders)
      .then(function (response) {
        // Clear the timeout as cleanup
        clearTimeout(timeout);
        if (!didTimeOut) resolve(response);
      })
      .catch(function (err) {
        // Rejection already happened with setTimeout
        if (didTimeOut) return;
        // Reject with error
        reject(err);
      });
  });
}

/**
 * This function serves to be a sub function that provides the actual fetch request. It takes most of the same data
 * as it's (arguable) superior function and performs the communication with the server.
 * @param url This is the url that is going to be fetched to.
 * @param method This is the (optional) method with which the fetch is going to be performed (e.g. get and so on).
 * @param token This is (optional) the authentication token required for a lot of fetch requests in the backend.
 * @param body The (optional) body of the request.
 * @param overrideHeaders
 * @private
 * @return Returns a response object containing the information returned from the server.
 */
async function _fetchRequest(
  url: string,
  method: 'get' | 'post' | 'put' | 'delete' = 'get',
  token?: string,
  // eslint-disable-next-line @typescript-eslint/ban-types
  body?: {},
  // eslint-disable-next-line @typescript-eslint/ban-types
  overrideHeaders: Headers | {} = {},
): Promise<Response> {
  const fetchObj: RequestInit = {
    method,
    headers: {
      'Content-Type': 'application/json',
      ...overrideHeaders,
    } as HeadersInit,
  };

  // for optional authentication set the token if given, otherwise omit it.
  if (token)
    fetchObj.headers = {
      ...fetchObj.headers,
      Authorization: `Bearer ${token}`,
    };

  // the body is also optional.
  if (body) fetchObj.body = JSON.stringify(body);

  return await fetch(`${environment.backendURL}${url}`, fetchObj);
}
