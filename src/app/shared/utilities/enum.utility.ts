/**
 * This function checks whether a given value is part of an enum that is also given.
 * @param _enum This is the enum which is supposed to be checked for the value.
 * @param value This is the value for which a check is supposed to occur regarding whether it is part of the enum.
 * @return Returns either the found corresponding value from the enum or an undefined if the value is not part of the enum.
 */
// eslint-disable-next-line
export function isValueOfEnum<T>(_enum: any, value: any): T | undefined {
  // don't use if(!value) because 0/false will return a true but it is legit
  if (value === undefined || value === null || !_enum) return undefined;

  const foundObj = Object.values(_enum).find(
    (c: any) => c.toString() === value.toString(),
  );
  if (foundObj !== undefined) return foundObj as T;

  return undefined;
}

export function bitIsSet<T extends number>(value: number, bit: T): boolean {
  return (value & bit) === bit;
}

export function bitsEquals<T extends number>(
  value: number,
  ...bits: T[]
): boolean {
  let toCheck = 0;
  bits.forEach((b) => (toCheck |= b));
  return value === toCheck;
}
