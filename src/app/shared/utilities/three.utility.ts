/**
 * Gets a string with letter spacing.
 * @remark
 *         Three.js does not support letter spacing. For this reason, the fonts
 *         for the player names etc. have a character at a certain position,
 *         which is exactly 1 long. This element can be appended to a character
 *         any number of times to increase the spacing between the characters.
 * @param text
 * @param spacing
 */
export function getTextWithLetterSpacing(
  text: string,
  spacing: number,
): string {
  if (spacing <= 0) return text;

  const workaroundSpacing = new Array(spacing).fill('\u00A0').join('');

  return text.split('').join(workaroundSpacing);
}
