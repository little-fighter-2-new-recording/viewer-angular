import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IpcService } from '../core/services/ipc.service';
import { DisableDirective } from './directives/disable.directive';

@NgModule({
  declarations: [DisableDirective],
  imports: [CommonModule],
  providers: [IpcService],
  exports: [DisableDirective],
})
export class SharedModule {}
