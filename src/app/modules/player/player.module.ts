import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerComponent } from './player.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSliderModule } from '@angular/material/slider';
import { MatIconModule } from '@angular/material/icon';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';
import { PlayerControlsComponent } from './player-controls/player-controls.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material/grid-list';
import { SharedModule } from '../../shared/shared.module';
import { RendererModule } from './renderer/renderer.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScenePlayerComponent } from './scene-player/scene-player.component';

@NgModule({
  declarations: [
    PlayerComponent,
    PlayerControlsComponent,
    LoadingDialogComponent,
    ScenePlayerComponent,
  ],
  providers: [],
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    MatButtonModule,
    MatSliderModule,
    MatIconModule,
    MatGridListModule,
    MatInputModule,
    SharedModule,
    RendererModule,
  ],
})
export class PlayerModule {}
