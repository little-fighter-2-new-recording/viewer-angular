import {
  BufferGeometry,
  DoubleSide,
  InstancedBufferAttribute,
  PlaneBufferGeometry,
  ShaderMaterial,
  Texture,
} from 'three';
import { Color } from 'three/src/math/Color';
import ThreeJsInstancedObject from './three-js-instanced-object';
import { ALPHA_TEST } from './constants';
import { ThreeDPosition } from '../../../../shared/models/three-d-position.model';
import { VERTEX_SHADER_SPRITESHEET_INSTANCE_POSITIONS } from './shaders/spritesheet-instance-positions.vertex';
import { FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY } from './shaders/spritesheet-chroma-key.fragment';

export default abstract class ThreeJsInstancedImageObject extends ThreeJsInstancedObject<
  BufferGeometry,
  ShaderMaterial
> {
  protected readonly texture: Texture;
  protected readonly textureWidth: number;
  protected readonly textureHeight: number;

  protected positions: InstancedBufferAttribute | undefined;

  protected textureWidthHalf: number;
  protected textureHeightHalf: number;

  protected constructor(
    texture: Texture,
    maxRenderCount: number,
    fallbackWidth?: number,
    fallbackHeight?: number,
  ) {
    super(maxRenderCount);

    if (!texture.image && !fallbackWidth)
      throw new Error('Invalid sprite width!');
    if (!texture.image && !fallbackHeight)
      throw new Error('Invalid sprite height!');

    this.texture = texture;
    this.textureWidth = this.texture.image
      ? this.texture.image.width
      : fallbackWidth;
    this.textureHeight = this.texture.image
      ? this.texture.image.height
      : fallbackHeight;

    this.textureWidthHalf = this.textureWidth / 2;
    this.textureHeightHalf = this.textureHeight / 2;
  }

  protected generateGeometry(): BufferGeometry {
    const geometry = new PlaneBufferGeometry(
      this.textureWidth,
      this.textureHeight,
    );

    const attributes = this.generateAttributes();
    for (const key of Object.keys(attributes)) {
      geometry.setAttribute(key, attributes[key]);
    }

    return geometry;
  }

  protected generateMaterial(): ShaderMaterial {
    return new ShaderMaterial({
      uniforms: {
        texture1: { value: this.texture },
        chromaKey: { value: new Color('#000000') },
        alphaTest: { value: ALPHA_TEST }, // see https://github.com/mrdoob/three.js/issues/15654
      },
      alphaTest: ALPHA_TEST,
      vertexShader: VERTEX_SHADER_SPRITESHEET_INSTANCE_POSITIONS,
      fragmentShader: FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY,
      side: DoubleSide,
    });
  }

  protected generateAttributes(): { [key: string]: InstancedBufferAttribute } {
    this.positions = new InstancedBufferAttribute(
      new Float32Array(3 * this.maxRenderCount),
      3,
    );
    return { instancePosition: this.positions };
  }

  protected setPosition(index: number, pos: ThreeDPosition) {
    this.positions?.setXYZ(index, pos.x, pos.y, pos.z);
  }

  protected setPositionLeftTop(index: number, pos: ThreeDPosition) {
    this.positions?.setXYZ(
      index,
      pos.x + this.textureWidthHalf,
      pos.y - this.textureHeightHalf,
      pos.z,
    );
  }

  protected updateBufferAttributes(): void {
    if (this.positions) this.positions.needsUpdate = true;
  }
}
