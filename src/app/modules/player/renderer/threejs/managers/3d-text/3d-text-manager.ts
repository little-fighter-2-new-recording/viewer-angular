import { Box3, ColorRepresentation, MeshStandardMaterial } from 'three';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import Manager from '../manager';
import ThreeDText from './3d-text';
import SimpleText from './simple-text';
import { LITTLE_FIGHTER_FONT_NEW_LINE_SPACING } from '../../constants';
import { IAnchorSetter } from '../../anchor-setter.interface';
import { Color } from 'three/src/math/Color';

export default class ThreeDTextManager
  extends Manager<
    string,
    ThreeDText | SimpleText,
    TextGeometry,
    MeshStandardMaterial
  >
  implements IAnchorSetter {
  private readonly _backFont: Font;
  private readonly _foreFont: Font;
  private _backColor: ColorRepresentation;
  private _foreColor: ColorRepresentation;
  private readonly _threeTimes3DEffect: boolean;
  private readonly _spacing: number;
  private _oldText: string;

  public constructor(
    text: string,
    backFont: Font,
    foreFont: Font,
    backColor: ColorRepresentation,
    foreColor: ColorRepresentation,
    threeTimes3DEffect = false,
    spacing = 0,
  ) {
    super();

    this._oldText = text;
    this._backFont = backFont;
    this._foreFont = foreFont;
    this._backColor = backColor;
    this._foreColor = foreColor;
    this._threeTimes3DEffect = threeTimes3DEffect;
    this._spacing = spacing;

    this.generateTextGeometries(text);
  }

  public getBoundingBox(): Box3 | undefined {
    const foreText = this.objects.find((o) => o.equalsFont(this._foreFont));
    if (!foreText) return undefined;

    foreText.geometry.computeBoundingBox();
    const box = foreText.geometry.boundingBox;
    if (!box) return undefined;

    box.min.y = 0.0; // coz else a strange offset for names with "g" (for example)

    return box;
  }

  public update(text: string): void {
    if (this._oldText === text) return;

    this.generateTextGeometries(text);
  }

  public updatePosition(x: number, y: number): void {
    this.group.position.set(x, y, this.group.position.z);
  }

  public updateColorBorder(newColor: ColorRepresentation): void {
    if (this._backColor === newColor) return;

    this.objects
      .filter((o) => o.equalsFont(this._backFont))
      .forEach((o) => (o.material.color = new Color(newColor)));

    this._backColor = newColor;
  }

  public updateColorFore(newColor: ColorRepresentation): void {
    if (this._foreColor === newColor) return;

    this.objects
      .filter((o) => o.equalsFont(this._foreFont))
      .forEach((o) => (o.material.color = new Color(newColor)));

    this._foreColor = newColor;
  }

  public updatePositionAnchorCenter(posX: number, posY: number): void {
    const box = this.getBoundingBox();
    if (!box) return;

    this.group.position.set(
      posX - Math.ceil((box.min.x + box.max.x) / 2),
      posY + Math.ceil((box.min.y + box.max.y) / 2),
      this.group.position.z,
    );
  }

  public updatePosAnchorRightBottom(
    maxX: number,
    maxY: number,
    posX: number,
    posY: number,
  ): void {
    const foreText = this.objects.find((o) => o.equalsFont(this._foreFont));
    if (!foreText) return;

    foreText.geometry.computeBoundingBox();
    const box = foreText.geometry.boundingBox;
    if (!box) return;

    this.group.position.set(
      maxX - Math.ceil(box.min.x + box.max.x) - posX,
      maxY + Math.ceil(box.min.y + box.max.y) + posY,
      this.group.position.z,
    );
  }

  private generateTextGeometries(text: string): void {
    this.group.clear();
    this.objects.splice(0);
    let posY = 0;

    for (const line of text.split(/\r?\n/)) {
      this.generateLineTextGeometries(line, posY);
      posY -= LITTLE_FIGHTER_FONT_NEW_LINE_SPACING;
    }

    this._oldText = text;
    this.initGroupFromObjects();
  }

  private generateLineTextGeometries(line: string, posY: number): void {
    const objects: (SimpleText | ThreeDText)[] = [];

    if (this._threeTimes3DEffect)
      objects.push(
        new ThreeDText(line, this._backFont, this._backColor, this._spacing),
      );
    else
      objects.push(
        new SimpleText(line, this._backFont, this._backColor, this._spacing),
      );

    const simpleText = new SimpleText(
      line,
      this._foreFont,
      this._foreColor,
      this._spacing,
    );
    simpleText.mesh.position.set(0, 0, 1);
    objects.push(simpleText);

    objects.forEach((o) =>
      o.mesh.position.set(
        o.mesh.position.x,
        o.mesh.position.y + posY,
        o.mesh.position.z,
      ),
    );

    this.objects.push(...objects);
  }
}
