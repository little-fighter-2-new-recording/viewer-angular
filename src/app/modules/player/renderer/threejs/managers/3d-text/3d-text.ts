import {
  ColorRepresentation,
  InstancedMesh,
  MeshStandardMaterial,
} from 'three';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import ThreeJsInstancedObject from '../../three-js-instanced-object';
import { Position } from '../../../../../../shared/models/position.model';
import { getTextWithLetterSpacing } from '../../../../../../shared/utilities/three.utility';
import {
  LITTLE_FIGHTER_FONT_CURVE_SEGMENTS,
  LITTLE_FIGHTER_FONT_SIZE,
  LITTLE_FIGHTER_FONT_SIZE_HEIGHT,
} from '../../constants';

// i love you marti...
const OFFSETS: Position[] = [
  {
    x: 0,
    y: 0,
  },
  { x: -1, y: -1 },
  { x: 0, y: -1 },
  { x: -1, y: 0 },
];

export default class ThreeDText extends ThreeJsInstancedObject<
  TextGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: TextGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: InstancedMesh;

  private readonly _text: string;
  private readonly _color: ColorRepresentation;
  private readonly _font: Font;
  private readonly _spacedText: string;

  public constructor(
    text: string,
    font: Font,
    color: ColorRepresentation,
    spacing = 0,
  ) {
    super(OFFSETS.length);

    this._text = text;
    this._font = font;
    this._color = color;
    this._spacedText = getTextWithLetterSpacing(text, spacing);

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();

    let offsetIndex = 0;
    const posYFactor = 1 / OFFSETS.length;
    for (const offset of OFFSETS) {
      this.tmpObject.position.set(offset.x, offset.y, offsetIndex * posYFactor);
      this.tmpObject.updateMatrix();
      this.mesh.setMatrixAt(offsetIndex++, this.tmpObject.matrix);
    }

    this.mesh.count = OFFSETS.length;
  }

  public equalsFont(font: Font): boolean {
    return this._font === font;
  }

  protected generateGeometry(): TextGeometry {
    return new TextGeometry(this._spacedText, {
      font: this._font,
      size: LITTLE_FIGHTER_FONT_SIZE,
      height: LITTLE_FIGHTER_FONT_SIZE_HEIGHT,
      curveSegments: LITTLE_FIGHTER_FONT_CURVE_SEGMENTS,
    });
  }

  protected generateMaterial(): MeshStandardMaterial {
    return new MeshStandardMaterial({
      color: this._color,
    });
  }
}
