import {
  BufferGeometry,
  ColorRepresentation,
  Material,
  Mesh,
  MeshStandardMaterial,
} from 'three';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import ThreeJsObject from '../../three-js-object';
import { getTextWithLetterSpacing } from '../../../../../../shared/utilities/three.utility';
import {
  LITTLE_FIGHTER_FONT_CURVE_SEGMENTS,
  LITTLE_FIGHTER_FONT_SIZE,
  LITTLE_FIGHTER_FONT_SIZE_HEIGHT,
} from '../../constants';

export default class SimpleText extends ThreeJsObject<
  TextGeometry,
  MeshStandardMaterial,
  Mesh
> {
  public readonly geometry: TextGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: Mesh;

  protected readonly _text: string;
  protected readonly _color: ColorRepresentation;
  protected readonly _font: Font;
  private readonly _spacedText: string;

  public constructor(
    text: string,
    font: Font,
    color: ColorRepresentation,
    spacing = 0,
  ) {
    super();

    this._text = text;
    this._font = font;
    this._color = color;
    this._spacedText = getTextWithLetterSpacing(text, spacing);

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public equalsFont(font: Font): boolean {
    return this._font === font;
  }

  protected generateGeometry(): TextGeometry {
    return new TextGeometry(this._spacedText, {
      font: this._font,
      size: LITTLE_FIGHTER_FONT_SIZE,
      height: LITTLE_FIGHTER_FONT_SIZE_HEIGHT,
      curveSegments: LITTLE_FIGHTER_FONT_CURVE_SEGMENTS,
    });
  }

  protected generateMaterial(): MeshStandardMaterial {
    return new MeshStandardMaterial({
      color: this._color,
    });
  }

  protected generateMesh(): Mesh {
    return new Mesh<BufferGeometry, Material | Material[]>(
      this.geometry,
      this.material,
    );
  }
}
