import { ColorRepresentation, MeshStandardMaterial } from 'three';
import Manager from '../manager';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import ThreeDText from '../3d-text/3d-text';
import SimpleText from '../3d-text/simple-text';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import ThreeDTextManager from '../3d-text/3d-text-manager';
import { PlayerNames } from '../../../../../../core-lib/models/player-names.model';
import { NAME_COLORS } from '../../../models/name-colors';
import SceneCharacter from '../../../../../../core-lib/models/scene-character.model';
import {
  AI_COM_START,
  OBJECTS_NAME_POSITION_OFFSET_Z,
  OBJECTS_START_POSITION_Z,
} from '../../../../../../shared/constants/little-fighter.constants';

const LITTLE_FIGHTER_TEAM_INDEPENDENT = 5;
const LITTLE_FIGHTER_MAXIMUM_BARS_OBJECT_ID = AI_COM_START * 2; // coz 0-10 real player and 10-20 ai player that are represents in "bars"

type UpdateData = {
  objects: SceneObject[];
  cameraX: number;
};

export default class NameManager extends Manager<
  UpdateData,
  ThreeDText | SimpleText,
  TextGeometry,
  MeshStandardMaterial
> {
  private readonly _backFont: Font;
  private readonly _foreFont: Font;
  private readonly _backColor: ColorRepresentation;
  private readonly _foreColor: ColorRepresentation;
  private readonly _playerNames: PlayerNames;

  private readonly _objectToName: { [key: number]: ThreeDTextManager } = {};

  public constructor(
    recordingNames: PlayerNames,
    backFont: Font,
    foreFont: Font,
    backColor: ColorRepresentation,
    foreColor: ColorRepresentation,
  ) {
    super();

    this._playerNames = recordingNames;
    this._backFont = backFont;
    this._foreFont = foreFont;
    this._backColor = backColor;
    this._foreColor = foreColor;

    this.group.position.set(0, 0, OBJECTS_START_POSITION_Z);
  }

  public update(data: UpdateData): void {
    data.objects = data.objects.filter(
      (d) =>
        (d.objectID >= 0 &&
          d.objectID < LITTLE_FIGHTER_MAXIMUM_BARS_OBJECT_ID) ||
        (d.dataID >= 0 && d.dataID < 30) || // lf2 magic numbers there have "com"
        (d.dataID >= 40 && d.dataID <= 55) || // lf2 magic numbers there have "com"
        (d instanceof SceneCharacter &&
          d.team < LITTLE_FIGHTER_TEAM_INDEPENDENT),
    );

    this.deleteRemovedSceneObjects(data.objects);

    for (const sceneObject of data.objects) {
      let manager = this._objectToName[sceneObject.objectID];
      const name = this._playerNames[sceneObject.objectID] ?? 'Com';
      if (!manager) {
        manager = new ThreeDTextManager(
          name,
          this._backFont,
          this._foreFont,
          this._backColor,
          this._foreColor,
          false,
          1,
        );
        this._objectToName[sceneObject.objectID] = manager;
        this.group.add(manager.getGroup());
      } else {
        manager.update(name);
      }

      const colors =
        NAME_COLORS[(sceneObject as SceneCharacter).team] ?? NAME_COLORS[0];

      manager.updateColorBorder(colors.border);
      manager.updateColorFore(colors.color);
      manager.updatePositionAnchorCenter(sceneObject.x, -sceneObject.z - 22);
      const grp = manager.getGroup();

      grp.position.setZ(sceneObject.z + OBJECTS_NAME_POSITION_OFFSET_Z);
      grp.position.setX(Math.max(grp.position.x, data.cameraX)); // LF displays the name in full width

      const box = manager.getBoundingBox();
      if (!box) return;
      const width = box.min.x + box.max.x;
      grp.position.setX(
        Math.min(data.cameraX + 794 - width - 1, grp.position.x),
      );
    }
  }

  private deleteRemovedSceneObjects(objects: SceneObject[]): void {
    const removedSceneIndices = Object.keys(this._objectToName)
      .map((i) => parseInt(i))
      .map((o) => (!objects.find((sO) => sO.objectID === o) ? o : -1))
      .filter((o) => o >= 0)
      .sort((o1, o2) => o2 - o1);

    while (removedSceneIndices.length > 0) {
      const sceneObjectIndex = removedSceneIndices.splice(0, 1)[0];
      this.group.remove(this._objectToName[sceneObjectIndex].getGroup());
      delete this._objectToName[sceneObjectIndex];
    }
  }
}
