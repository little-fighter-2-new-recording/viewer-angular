import {
  ColorRepresentation,
  InstancedMesh,
  MeshBasicMaterial,
  PlaneBufferGeometry,
} from 'three';
import { Color } from 'three/src/math/Color';
import ThreeJsInstancedObject from '../../three-js-instanced-object';
import {
  BAR_HEIGHT,
  BAR_POSITION_X,
} from '../../../../../../shared/constants/little-fighter.constants';
import { Position } from '../../../../../../shared/models/position.model';

export class BarObject extends ThreeJsInstancedObject<
  PlaneBufferGeometry,
  MeshBasicMaterial
> {
  public readonly geometry: PlaneBufferGeometry;
  public readonly material: MeshBasicMaterial;
  public readonly mesh: InstancedMesh;

  private readonly _color: ColorRepresentation;
  private readonly _zIndex: number;

  public constructor(
    color: ColorRepresentation,
    maxRenderCount: number,
    relativeYPosInBar: number,
    zIndex: number,
  ) {
    super(maxRenderCount);

    this._color = color;
    this._zIndex = zIndex;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();

    this.mesh.position.set(
      BAR_POSITION_X,
      -relativeYPosInBar - BAR_HEIGHT / 2,
      0,
    );

    this.mesh.count = maxRenderCount;
  }

  public changeWidth(index: number, newWidth: number, barPos: Position): void {
    this.tmpObject.position.set(
      barPos.x + newWidth / 2,
      -barPos.y,
      index + this._zIndex,
    );
    this.tmpObject.scale.set(newWidth, 1, 1);
    this.tmpObject.updateMatrix();

    this.mesh.setMatrixAt(index, this.tmpObject.matrix);
    this.mesh.instanceMatrix.needsUpdate = true;
  }

  protected generateGeometry(): PlaneBufferGeometry {
    return new PlaneBufferGeometry(1, BAR_HEIGHT);
  }

  protected generateMaterial(): MeshBasicMaterial {
    return new MeshBasicMaterial({ color: new Color(this._color) });
  }
}
