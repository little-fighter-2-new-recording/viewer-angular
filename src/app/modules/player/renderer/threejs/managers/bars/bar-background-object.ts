import { BufferGeometry, InstancedMesh, ShaderMaterial, Texture } from 'three';
import ThreeJsInstancedImageObject from '../../three-js-instanced-image-object';
import {
  BAR_BACKGROUND_HEIGHT,
  BAR_BACKGROUND_WIDTH,
} from '../../../../../../shared/constants/little-fighter.constants';
import { IUpdatable } from '../../updatable.interface';
import { Position } from '../../../../../../shared/models/position.model';

export class BarBackgroundObject
  extends ThreeJsInstancedImageObject
  implements IUpdatable<Position[]> {
  public readonly geometry: BufferGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh;

  public constructor(texture: Texture, maxRenderCount: number) {
    super(texture, maxRenderCount, BAR_BACKGROUND_WIDTH, BAR_BACKGROUND_HEIGHT);

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public update(data: Position[]) {
    if (data.length > this.maxRenderCount) {
      console.error('Too many objects!');
      return;
    }

    if (data.length == 0) {
      this.mesh.visible = false;
      return;
    }

    for (let i = 0; i < data.length; ++i) {
      this.setPositionLeftTop(i, {
        x: data[i].x,
        y: -data[i].y, // minus coz reverse coordinate system
        z: i,
      });
    }

    this.updateBufferAttributes();
    this.mesh.visible = true;
    this.mesh.count = data.length;
  }
}
