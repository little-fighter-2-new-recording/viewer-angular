import { Texture } from 'three';
import Manager from '../manager';
import { Position } from '../../../../../../shared/models/position.model';
import ThreeJsObject from '../../three-js-object';
import { BarBackgroundObject } from './bar-background-object';
import {
  AI_COM_START,
  BAR_COLOR_HP,
  BAR_COLOR_HP_DARK,
  BAR_COLOR_MP,
  BAR_COLOR_MP_DARK,
  BAR_MAX_WIDTH,
  BAR_POSITION_BACK_INDEX_Z,
  BAR_POSITION_INDEX_Z,
  BAR_POSITION_Y,
  BAR_POSITION_Y2,
  BARS_MAXIMUM,
  BARS_POSITION_Z,
} from '../../../../../../shared/constants/little-fighter.constants';
import { IUpdatable } from '../../updatable.interface';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import ScenePlayer from '../../../../../../core-lib/models/scene-player.model';
import SceneCharacter from '../../../../../../core-lib/models/scene-character.model';
import BarSmallImageObject from './bar-small-image-object';
import { AssetService } from '../../../../../../core/services/asset.service';
import {
  LITTLE_FIGHTER_2_SMALL,
  SYSTEM_BAR_BACKGROUND,
} from '../../../../../../shared/constants/assets.constants';
import { BarObject } from './bar-object';
import { CharacterBarsService } from '../../../../../../core/services/character-bars.service';

const INDEX_HP_NORMAL = 0;
const INDEX_HP_DARK = 1;
const INDEX_MP_NORMAL = 2;
const INDEX_MP_DARK = 3;

export default class BarsManager extends Manager<
  SceneObject[],
  ThreeJsObject<any, any, any>
> {
  private readonly _lastPlayers: (SceneCharacter | undefined)[];
  private readonly _smallImages: BarSmallImageObject[] = [];

  public constructor(
    private readonly _assetService: AssetService,
    private readonly _barsService: CharacterBarsService,
  ) {
    super();

    this._lastPlayers = new Array(BARS_MAXIMUM).fill(undefined);

    for (let i = 0; i < BARS_MAXIMUM; ++i) {
      this._smallImages.push(new BarSmallImageObject(undefined, i + 0.1));
    }

    this.objects.push(
      new BarBackgroundObject(
        this._assetService.getTexture(SYSTEM_BAR_BACKGROUND),
        BARS_MAXIMUM,
      ),
    );

    this.objects.splice(
      INDEX_HP_NORMAL,
      0,
      new BarObject(
        BAR_COLOR_HP,
        BARS_MAXIMUM,
        BAR_POSITION_Y,
        BAR_POSITION_INDEX_Z,
      ),
    );
    this.objects.splice(
      INDEX_HP_DARK,
      0,
      new BarObject(
        BAR_COLOR_HP_DARK,
        BARS_MAXIMUM,
        BAR_POSITION_Y,
        BAR_POSITION_BACK_INDEX_Z,
      ),
    );
    this.objects.splice(
      INDEX_MP_NORMAL,
      0,
      new BarObject(
        BAR_COLOR_MP,
        BARS_MAXIMUM,
        BAR_POSITION_Y2,
        BAR_POSITION_INDEX_Z,
      ),
    );
    this.objects.splice(
      INDEX_MP_DARK,
      0,
      new BarObject(
        BAR_COLOR_MP_DARK,
        BARS_MAXIMUM,
        BAR_POSITION_Y2,
        BAR_POSITION_BACK_INDEX_Z,
      ),
    );
    this.objects.push(...this._smallImages);

    this.initGroupFromObjects();

    this.group.position.set(0, 0, BARS_POSITION_Z);

    this._barsService.barPositions$.subscribe((positions) =>
      this.updateBarPositions(positions),
    );

    this.updateBarPositions(this._barsService.barPositions);
  }

  private static isUpdatable(obj: any): obj is IUpdatable<any> {
    return obj.update;
  }

  public update(objects: SceneObject[]): void {
    for (let i = 0; i < BARS_MAXIMUM; ++i) {
      let obj = objects.find((o) => o.objectID == i);
      if (!obj) obj = objects.find((o) => o.objectID == i + AI_COM_START);

      if (!obj || !(obj instanceof ScenePlayer)) {
        this._smallImages[i].update(new Texture());
        this.hideBars(i);
        this._lastPlayers[i] = undefined;
      } else {
        const lastPlayer = this._lastPlayers[i];
        if (!lastPlayer || lastPlayer.dataID != obj.dataID) {
          const newSmallImage = this._assetService.getTexture(
            `${LITTLE_FIGHTER_2_SMALL}_${obj.dataID}`,
          );
          this._smallImages[i].update(newSmallImage);
        }

        this.updateBarValues(i, obj);
        this._lastPlayers[i] = obj;
      }
    }
  }

  private updateBarPositions(data: Position[]): void {
    (<BarBackgroundObject[]>(
      this.objects.filter((o) => o instanceof BarBackgroundObject)
    )).forEach((o) => o.update(data));

    const smalls = <BarSmallImageObject[]>(
      this.objects.filter((o) => o instanceof BarSmallImageObject)
    );

    for (let i = 0; i < Math.min(smalls.length, data.length); ++i) {
      this._smallImages[i].updateTest(data[i]);
    }

    for (let i = 0; i < BARS_MAXIMUM; ++i) {
      const obj = this._lastPlayers[i];
      if (!obj || !(obj instanceof ScenePlayer)) {
        this.hideBars(i);
      } else {
        this.updateBarValues(i, obj);
      }
    }
  }

  private updateBarValues(index: number, character: ScenePlayer): void {
    const bars = <BarObject[]>(
      this.objects.filter((o) => o instanceof BarObject)
    );
    if (character.hp <= 0) {
      this.hideBars(index);
      return;
    }

    const barPosition = this._barsService.barPositions[index];
    const maxHP = BAR_MAX_WIDTH / character.hpMax;
    bars[INDEX_HP_NORMAL].changeWidth(index, maxHP * character.hp, barPosition);
    bars[INDEX_HP_DARK].changeWidth(
      index,
      maxHP * character.hpDark,
      barPosition,
    );

    const maxMP = BAR_MAX_WIDTH / 500;
    bars[INDEX_MP_NORMAL].changeWidth(index, maxMP * character.mp, barPosition);
    bars[INDEX_MP_DARK].changeWidth(index, maxMP * 500, barPosition);
  }

  private hideBars(index: number): void {
    const bars = <BarObject[]>(
      this.objects.filter((o) => o instanceof BarObject)
    );
    bars.forEach((b) => b.changeWidth(index, 0, { x: 0, y: 0 })); // remark: position is useless, so just 0,0
  }
}
