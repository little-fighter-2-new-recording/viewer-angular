import { Texture } from 'three';
import ThreeJsImageObject from '../../three-js-image-object';
import { Position } from '../../../../../../shared/models/position.model';
import {
  BAR_SMALL_POSITION_X,
  BAR_SMALL_POSITION_Y,
  SMALL_SIZE_HEIGHT,
  SMALL_SIZE_WIDTH,
} from '../../../../../../shared/constants/little-fighter.constants';

export default class BarSmallImageObject extends ThreeJsImageObject {
  private readonly _zIndex: number;

  public constructor(texture: Texture | undefined, zIndex: number) {
    super(texture ?? new Texture(), SMALL_SIZE_WIDTH, SMALL_SIZE_HEIGHT);

    this._zIndex = zIndex;
  }

  public updateTest(barPos: Position): void {
    this.setPositionLeftTop({
      x: barPos.x + BAR_SMALL_POSITION_X,
      y: -barPos.y - BAR_SMALL_POSITION_Y,
      z: this._zIndex,
    });
  }

  public update(texture: Texture | undefined): void {
    this.mesh.visible = !!texture;

    if (texture) this.material.uniforms['texture1'].value = texture;
  }
}
