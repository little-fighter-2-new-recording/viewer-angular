import { MeshStandardMaterial, PlaneBufferGeometry } from 'three';
import Manager from '../manager';
import BloodObject from './blood-object';
import { SceneObjectFrameSprite } from './models/scene-object-frame-sprite.model';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import { RecordingService } from '../../../../../../core/services/recording.service';
import ScenePlayer from '../../../../../../core-lib/models/scene-player.model';
import { isObjectVisible } from '../../../../../../shared/utilities/lf.utility';

const HP_LOST_TO_BLOOD = 0.33; // in percent

export default class BloodManager extends Manager<
  SceneObject[],
  BloodObject,
  PlaneBufferGeometry,
  MeshStandardMaterial
> {
  public constructor(
    private readonly _recordingService: RecordingService,
    maxRenderCount: number,
  ) {
    super();

    this.objects.push(new BloodObject(maxRenderCount));
    this.initGroupFromObjects();
  }

  public update(data2: SceneObject[]): void {
    const data = data2
      .filter(
        (d) =>
          d instanceof ScenePlayer &&
          d.hp < Math.ceil(d.hpMax * HP_LOST_TO_BLOOD) &&
          isObjectVisible(d),
      )
      .map((d) => {
        const frame = this._recordingService.getFrame(d.dataID, d.frameID);
        return {
          ...d,
          frame: frame,
          spriteSize: this._recordingService.getSpritesheetSize(
            d.dataID,
            (frame?.picID ?? 0) + d.picGain,
          ),
        } as SceneObjectFrameSprite;
      });
    const players = data.filter(
      (d) => d.frame.bloodX > 0 && d.frame.bloodY > 0,
    );
    this.objects.forEach((o) => o.update(players));
  }
}
