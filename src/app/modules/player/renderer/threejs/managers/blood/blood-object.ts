import ThreeJsInstancedObject from '../../three-js-instanced-object';
import {
  InstancedMesh,
  MeshStandardMaterial,
  PlaneBufferGeometry,
} from 'three';
import { OBJECTS_CHARACTER_BLOOD_POSITION_OFFSET_Z } from '../../../../../../shared/constants/little-fighter.constants';
import { SceneObjectFrameSprite } from './models/scene-object-frame-sprite.model';

export default class BloodObject extends ThreeJsInstancedObject<
  PlaneBufferGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: PlaneBufferGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: InstancedMesh;

  public constructor(maxRenderCount: number) {
    super(maxRenderCount);

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public update(objects: SceneObjectFrameSprite[]): void {
    if (objects.length > this.maxRenderCount) {
      console.error('Too many objects!');
      return;
    }

    if (objects.length == 0) {
      this.mesh.visible = false;
      return;
    }

    for (let i = 0; i < objects.length; ++i) {
      const object = objects[i];
      let drawX = !object.facing
        ? object.x - object.frame.centerX + object.frame.bloodX + 1
        : object.x -
          object.spriteSize.width +
          object.frame.centerX +
          (object.spriteSize.width - object.frame.bloodX);

      const drawY =
        -object.y + object.frame.centerY - object.frame.bloodY - object.z - 1;

      this.tmpObject.position.set(
        drawX,
        drawY,
        object.z + OBJECTS_CHARACTER_BLOOD_POSITION_OFFSET_Z,
      );
      this.tmpObject.updateMatrix();
      this.mesh.setMatrixAt(i, this.tmpObject.matrix);
    }

    this.mesh.count = objects.length;
    this.mesh.instanceMatrix.needsUpdate = true;
    this.mesh.visible = true;
  }

  protected generateGeometry(): PlaneBufferGeometry {
    return new PlaneBufferGeometry(1, 3);
  }

  protected generateMaterial(): MeshStandardMaterial {
    return new MeshStandardMaterial({ color: '#ff0000' });
  }
}
