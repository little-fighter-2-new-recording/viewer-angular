import { SceneObjectWithFrame } from '../../../models/scene-object-with-frame.model';
import { Size } from '../../../../../../../shared/models/size.model';

export type SceneObjectFrameSprite = SceneObjectWithFrame & {
  spriteSize: Size;
};
