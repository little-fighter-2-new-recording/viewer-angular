import ThreeJsInstancedImageObject from '../../three-js-instanced-image-object';
import { BufferGeometry, InstancedMesh, ShaderMaterial, Texture } from 'three';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import { OBJECTS_SHADOW_POSITION_OFFSET_Z } from '../../../../../../shared/constants/little-fighter.constants';

export default class ShadowObject extends ThreeJsInstancedImageObject {
  public readonly geometry: BufferGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh;

  public constructor(texture: Texture, maxRenderCount: number) {
    super(texture, maxRenderCount);

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public update(objects: SceneObject[]): void {
    if (objects.length > this.maxRenderCount) {
      console.error('Too many objects!');
      return;
    }

    if (objects.length == 0) {
      this.mesh.visible = false;
      return;
    }

    for (let i = 0; i < objects.length; ++i) {
      this.setPosition(i, {
        x: objects[i].x,
        y: -objects[i].z,
        z: objects[i].z + OBJECTS_SHADOW_POSITION_OFFSET_Z,
      });
    }

    this.mesh.visible = true;
    this.mesh.count = objects.length;
    this.mesh.instanceMatrix.needsUpdate = true;
    this.updateBufferAttributes();
  }
}
