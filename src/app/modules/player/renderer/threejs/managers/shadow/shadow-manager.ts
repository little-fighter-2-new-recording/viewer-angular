import { Texture } from 'three';
import Manager from '../manager';
import Background from '../../../../../../core-lib/models/background.model';
import ShadowObject from './shadow-object';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import { OBJECTS_START_POSITION_Z } from '../../../../../../shared/constants/little-fighter.constants';
import { isObjectVisible } from '../../../../../../shared/utilities/lf.utility';

export default class ShadowManager extends Manager<
  SceneObject[],
  ShadowObject
> {
  private readonly _bg: Background;
  private readonly _shadow: Texture;

  public constructor(bg: Background, shadow: Texture, maxRenderCount: number) {
    super();
    if (!shadow.image) throw new Error();

    this._bg = bg;
    this._shadow = shadow;

    this.objects.push(new ShadowObject(this._shadow, maxRenderCount));
    this.initGroupFromObjects();

    const drawX =
      Math.floor(this._bg.shadowWidth / 2) + 1 - this._shadow.image.width / 2;
    const drawY =
      Math.floor(this._bg.shadowHeight / 2) - this._shadow.image.height / 2;

    this.group.position.set(drawX, drawY, OBJECTS_START_POSITION_Z);
  }

  public update(data: SceneObject[]): void {
    const objectsWithShadow = data.filter(
      (s) => s.shadow && s.invisible === 0 && isObjectVisible(s),
    );

    this.objects.forEach((o) => o.update(objectsWithShadow));
  }
}
