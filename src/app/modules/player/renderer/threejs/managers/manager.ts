import { BufferGeometry, Group, Material, Mesh, ShaderMaterial } from 'three';
import ThreeJsObject from '../three-js-object';
import { Geometry } from 'three/examples/jsm/deprecated/Geometry';
import { Object3D } from 'three/src/core/Object3D';

export default abstract class Manager<
  TInput,
  TObject extends ThreeJsObject<TGeometry, TMaterial, TMesh>,
  TGeometry extends Geometry | BufferGeometry = BufferGeometry,
  TMaterial extends Material = ShaderMaterial,
  TMesh extends Mesh | Object3D = Mesh,
> {
  protected readonly group: Group;
  protected readonly objects: TObject[] = [];

  protected constructor() {
    this.group = new Group();
  }

  public getGroup(): Group {
    return this.group;
  }

  protected initGroupFromObjects(): void {
    for (const obj of this.objects) this.group.add(obj.mesh);
  }

  public abstract update(data: TInput): void;
}
