import Manager from '../manager';
import Layer from '../../../../../../core-lib/models/layer.model';
import {
  BACKGROUND_START_POSITION_Z,
  VIEWER_VIEW_PORT_WIDTH,
  WINDOW_WIDTH,
} from '../../../../../../shared/constants/little-fighter.constants';
import LayerImage from '../../../../../../core-lib/models/layer-image.model';
import LayerColor from '../../../../../../core-lib/models/layer-color.model';
import Background from '../../../../../../core-lib/models/background.model';
import { LayerImageObject } from './layer-image-object';
import LayerColorObject from './layer-color-object';
import { AssetService } from '../../../../../../core/services/asset.service';
import Scene from '../../../../../../core-lib/models/scene.model';
import StageScene from '../../../../../../core-lib/models/stage-scene.model';

type BackgroundDrawInfo = {
  cameraX: number;
  drawCounter: number;
};
export default class BackgroundManager extends Manager<
  BackgroundDrawInfo,
  LayerImageObject | LayerColorObject,
  any,
  any,
  any
> {
  private readonly _background: Background;

  public constructor(
    background: Background,
    private readonly _assetService: AssetService,
  ) {
    super();

    this._background = background;

    for (let index = 0; index < background.layers.length; ++index) {
      const layer = background.layers[index];
      const amountOfLayer =
        layer.loop <= 0 ? 1 : (background.width - layer.x) / layer.loop + 1;
      if (layer instanceof LayerImage) {
        const texture = this._assetService.getTexture(
          BackgroundManager.getLayerAssetPath(layer.path),
        );

        this.objects.push(
          new LayerImageObject(
            index,
            amountOfLayer,
            texture,
            layer,
            background.width,
          ),
        );
      } else if (layer instanceof LayerColor) {
        this.objects.push(
          new LayerColorObject(layer, background.width, index, amountOfLayer),
        );
      }
    }

    this.group.position.set(0, 0, BACKGROUND_START_POSITION_Z);
    this.initGroupFromObjects();
  }

  public static getLayerAssetPath(path: string): string {
    return `assets/lf2/${path}.png`;
  }

  public static getShadowAssetPath(path: string | undefined): string {
    return path ? `assets/lf2/${path}.png` : `/assets/lf2/bg/shadow.png`;
  }

  public static getDrawPosition(
    layer: Layer,
    loopIndex: number,
    scrollPositionX: number,
    bgWidth: number,
    imageWidth: number,
    imageHeight: number,
  ): { x: number; y: number } {
    const layerX = layer.x + imageWidth / 2;
    let drawX = scrollPositionX;
    const drawY = -layer.y - imageHeight / 2;

    if (bgWidth > WINDOW_WIDTH)
      drawX *= (layer.width - WINDOW_WIDTH) / (bgWidth - WINDOW_WIDTH);

    drawX = layerX + layer.loop * loopIndex - drawX;
    drawX += scrollPositionX;

    return { x: drawX, y: drawY };
  }

  public update(data: BackgroundDrawInfo): void {
    this.objects.forEach((o) => o.update(data.cameraX, data.drawCounter));
  }

  public getMaximumY(scene: Scene): number {
    if (scene instanceof StageScene)
      return (
        Math.min(this._background.width, scene.bound) - VIEWER_VIEW_PORT_WIDTH
      );

    return this._background.width - VIEWER_VIEW_PORT_WIDTH;
  }
}
