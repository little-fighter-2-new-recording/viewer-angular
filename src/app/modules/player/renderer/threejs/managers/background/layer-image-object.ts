import {
  BufferGeometry,
  InstancedMesh,
  NearestFilter,
  ShaderMaterial,
  Texture,
} from 'three';
import ThreeJsInstancedImageObject from '../../three-js-instanced-image-object';
import LayerImage from '../../../../../../core-lib/models/layer-image.model';
import Layer from '../../../../../../core-lib/models/layer.model';
import BackgroundManager from './background-manager';

export class LayerImageObject extends ThreeJsInstancedImageObject {
  public readonly geometry: BufferGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh;
  public readonly layerIndex: number;

  private readonly _layer: LayerImage;
  private readonly _bgWidth: number;

  public constructor(
    layerIndex: number,
    maxRenderCount: number,
    texture: Texture,
    layer: LayerImage,
    bgWidth: number,
  ) {
    super(texture, maxRenderCount);

    this.layerIndex = layerIndex;
    this._layer = layer;
    this._bgWidth = bgWidth;

    //  texture.wrapS = texture.wrapT = RepeatWrapping;
    // texture.repeat.set(1 / this._width, 1 / this._height);
    texture.minFilter = NearestFilter;
    texture.magFilter = NearestFilter;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public static isLayerVisible(layer: Layer, drawCounter: number): boolean {
    if (layer.cc <= 0) return true;

    const mod = drawCounter % layer.cc;
    return layer.c1 <= mod && mod <= layer.c2;
  }

  public update(scrollPosition: number, drawCounter: number): void {
    this.mesh.visible = LayerImageObject.isLayerVisible(
      this._layer,
      drawCounter,
    );

    if (!this.mesh.visible) return;

    for (let i = 0; i < this.maxRenderCount; ++i) {
      const { x, y } = BackgroundManager.getDrawPosition(
        this._layer,
        i,
        scrollPosition,
        this._bgWidth,
        this.textureWidth,
        this.textureHeight,
      );

      this.setPosition(i, { x, y, z: this.layerIndex });
    }

    this.mesh.count = this.maxRenderCount;
    this.mesh.instanceMatrix.needsUpdate = true;
    this.updateBufferAttributes();
  }
}
