import {
  InstancedMesh,
  MeshStandardMaterial,
  PlaneBufferGeometry,
} from 'three';
import { LayerImageObject } from './layer-image-object';
import LayerColorModel from '../../../../../../core-lib/models/layer-color.model';
import ThreeJsInstancedObject from '../../three-js-instanced-object';
import BackgroundManager from './background-manager';

export default class LayerColorObject extends ThreeJsInstancedObject<
  PlaneBufferGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: PlaneBufferGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: InstancedMesh;
  public readonly layerIndex: number;

  protected readonly imageHeight: number;
  protected readonly imageWidth: number;
  protected readonly maxRenderCount: number;

  private readonly _layer: LayerColorModel;
  private readonly _bgWidth: number;

  public constructor(
    layer: LayerColorModel,
    bgWidth: number,
    layerIndex: number,
    maxRenderCount: number,
  ) {
    super(maxRenderCount);

    this.layerIndex = layerIndex;
    this._layer = layer;
    this.imageWidth = layer.width;
    this.imageHeight = layer.height;
    this._bgWidth = bgWidth;
    this.maxRenderCount = maxRenderCount;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  public update(scrollPosition: number, drawCounter: number): void {
    this.mesh.visible = LayerImageObject.isLayerVisible(
      this._layer,
      drawCounter,
    );

    if (!this.mesh.visible) return;

    for (let i = 0; i < this.maxRenderCount; ++i) {
      this.setPosition(i, scrollPosition);
    }

    this.mesh.count = this.maxRenderCount;
    this.mesh.instanceMatrix.needsUpdate = true;
  }

  protected generateMaterial(): MeshStandardMaterial {
    return new MeshStandardMaterial({ color: this._layer.color });
  }

  protected generateGeometry(): PlaneBufferGeometry {
    return new PlaneBufferGeometry(this.imageWidth, this.imageHeight);
  }

  protected setPosition(index: number, scrollPosition: number): void {
    const { x, y } = BackgroundManager.getDrawPosition(
      this._layer,
      index,
      scrollPosition,
      this._bgWidth,
      this.imageWidth,
      this.imageHeight,
    );

    this.tmpObject.position.set(x, y, this.layerIndex);
    this.tmpObject.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpObject.matrix);
  }
}
