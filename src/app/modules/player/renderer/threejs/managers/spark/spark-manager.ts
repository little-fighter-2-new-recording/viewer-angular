import Manager from '../manager';
import SparkObject from './spark-object';
import SceneSpark from 'src/app/core-lib/models/spark.model';
import { SparkWithFrame } from '../../../models/spark-with-frame.model';
import { AssetService } from '../../../../../../core/services/asset.service';
import { SPARKS } from './sparks.constants';
import { SYSTEM_SPARK } from '../../../../../../shared/constants/assets.constants';
import {
  SPARK_MINIMUM_TO_DRAW,
  SPARK_START_POSITION_Z,
} from '../../../../../../shared/constants/little-fighter.constants';

const SPARK_MAXIMUM_BUFFER = 20; // multiplier, how often the maximum number of sparks can actually be drawn. Possibly the drawer tries to draw more than actually intended

export default class SparkManager extends Manager<SceneSpark[], SparkObject> {
  private readonly _activeSparks: SparkWithFrame[] = [];

  public constructor(
    private readonly _assetService: AssetService,
    maximalSparks: number,
  ) {
    super();

    const sparkMaximum = Math.max(
      SPARK_MINIMUM_TO_DRAW,
      maximalSparks * SPARK_MAXIMUM_BUFFER,
    );

    let posZ = SPARK_START_POSITION_Z;
    for (const spark of SPARKS) {
      const texture = this._assetService.getTexture(
        `${SYSTEM_SPARK}_${spark.path}`,
      );

      const sparkObj = new SparkObject(
        spark.id,
        texture,
        spark.width,
        spark.height,
        sparkMaximum,
      );
      sparkObj.mesh.position.set(0, 0, posZ++);
      this.objects.push(sparkObj);
    }

    this.initGroupFromObjects();

    this.group.position.set(0, 0, SPARK_START_POSITION_Z);
  }

  public update(data: SceneSpark[]): void {
    this._activeSparks.push(
      ...data.map((spark) => ({ spark, currentFrame: 0 })),
    );
  }

  public updateFrames(): void {
    for (const spark of this.objects) {
      spark.update(
        this._activeSparks.filter((s) => s.spark.frameID === spark.id),
      );
    }

    for (const spark of this._activeSparks) ++spark.currentFrame;

    let i = 0;
    while (i < this._activeSparks.length) {
      if (this._activeSparks[i].currentFrame === 5) {
        this._activeSparks.splice(i, 1);
      } else {
        ++i;
      }
    }
  }
}
