import { Texture } from 'three';
import ThreeJsLF2SpriteSheetObject from '../../three-js-lf2-sprite-sheet-object';
import { IUpdatable } from '../../updatable.interface';
import { SparkWithFrame } from '../../../models/spark-with-frame.model';

export default class SparkObject
  extends ThreeJsLF2SpriteSheetObject
  implements IUpdatable<SparkWithFrame[]> {
  public readonly id: number;

  public constructor(
    id: number,
    texture: Texture,
    spriteWidth: number,
    spriteHeight: number,
    maxRenderCount: number,
  ) {
    super(
      {
        texture: texture,
        spriteCol: 1,
        spriteRow: 5,
        spriteWidth,
        spriteHeight,
        picIndexStart: 0,
      },
      maxRenderCount,
    );

    this.id = id;
  }

  public update(sparks: SparkWithFrame[]): void {
    if (sparks.length > this.maxRenderCount) {
      console.error(
        `Too many objects! (${sparks.length} / ${this.maxRenderCount})`,
      );
      return;
    }

    if (sparks.length === 0) {
      this.mesh.visible = false;
      return;
    }

    const zIndexFactor = 1 / this.maxRenderCount;
    for (let i = 0; i < sparks.length; ++i) {
      const spark = sparks[i];
      this.setSprite(i, spark.currentFrame, false);
      this.setPositionLeftTop(i, {
        x: spark.spark.x,
        y: -spark.spark.y,
        z: i * zIndexFactor,
      });
    }

    this.updateBufferAttributes();

    this.mesh.visible = true;
    this.mesh.count = sparks.length;
  }
}
