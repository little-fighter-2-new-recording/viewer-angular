import * as LF2Constants from '../../../../../../shared/constants/little-fighter.constants';

export const SPARKS = [
  {
    width: LF2Constants.SPARK_SMALL_SPRITE_WIDTH,
    height: LF2Constants.SPARK_SMALL_SPRITE_HEIGHT,
    path: 'spark_hit_small',
    id: LF2Constants.SPARK_ID_HIT_SMALL,
  },
  {
    width: LF2Constants.SPARK_LARGE_SPRITE_WIDTH,
    height: LF2Constants.SPARK_LARGE_SPRITE_HEIGHT,
    path: 'spark_hit_big',
    id: LF2Constants.SPARK_ID_HIT_LARGE,
  },
  {
    width: LF2Constants.SPARK_SMALL_SPRITE_WIDTH,
    height: LF2Constants.SPARK_SMALL_SPRITE_HEIGHT,
    path: 'spark_blood_small',
    id: LF2Constants.SPARK_ID_BLOOD_SMALL,
  },
  {
    width: LF2Constants.SPARK_LARGE_SPRITE_WIDTH,
    height: LF2Constants.SPARK_LARGE_SPRITE_HEIGHT,
    path: 'spark_blood_big',
    id: LF2Constants.SPARK_ID_BLOOD_LARGE,
  },
];
