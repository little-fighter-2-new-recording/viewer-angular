import { BufferGeometry, InstancedMesh, ShaderMaterial } from 'three';
import ThreeJsLF2SpriteSheetObject from '../../three-js-lf2-sprite-sheet-object';
import { LF2Spritesheet } from '../../models/lf2-spritesheet.model';
import { OBJECTS_SPRITE_POSITION_OFFSET_Z } from '../../../../../../shared/constants/little-fighter.constants';
import { SceneObjectWithFrame } from '../../models/scene-object-with-frame.model';

export default class Lf2Object extends ThreeJsLF2SpriteSheetObject {
  public readonly geometry: BufferGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh;

  public readonly dataID: number;
  public readonly picIndexEnd: number;
  public readonly picIndexStart: number;

  public constructor(
    spritesheetInfo: LF2Spritesheet,
    dataID: number,
    maxRenderCount: number,
  ) {
    super(spritesheetInfo, maxRenderCount);

    this.dataID = dataID;
    this.picIndexStart = spritesheetInfo.picIndexStart ?? 0;
    this.picIndexEnd =
      spritesheetInfo.spriteCol * spritesheetInfo.spriteRow -
      1 +
      this.picIndexStart; // -1 because it starts by 0

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();

    this.mesh.position.set(this.spriteWidth / 2, -this.spriteHeight / 2, 0);
  }

  public shouldHandleSceneObject(obj: SceneObjectWithFrame): boolean {
    return (
      obj.dataID === this.dataID &&
      obj.frame.picID + obj.picGain >= this.picIndexStart &&
      obj.frame.picID + obj.picGain <= this.picIndexEnd
    );
  }

  public update(objects: SceneObjectWithFrame[]): void {
    if (objects.length > this.maxRenderCount) {
      console.error('Too many objects!');
      return;
    }

    if (objects.length == 0) {
      this.mesh.visible = false;
      return;
    }

    for (let i = 0; i < objects.length; ++i) {
      const object = objects[i];
      this.setSprite(i, object.frame.picID + object.picGain, object.facing);

      let drawX = !object.facing
        ? object.x - object.frame.centerX
        : object.x - this.spriteWidth + object.frame.centerX;

      const drawY = -object.y + object.frame.centerY - object.z;

      if (object.shake !== undefined && object.shake < 0)
        drawX -= 3 * (object.shake % 2 ? 1 : -1);

      this.setPosition(i, {
        x: drawX,
        y: drawY,
        z: object.z + OBJECTS_SPRITE_POSITION_OFFSET_Z,
      });
    }

    this.updateBufferAttributes();
    this.mesh.visible = true;
    this.mesh.count = objects.length;
  }
}
