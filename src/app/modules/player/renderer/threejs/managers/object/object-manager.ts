import Manager from '../manager';
import Lf2Object from './lf2-object';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import { RecordingFormat } from '../../../../../../core-lib/models/recording-format.model';
import { AssetService } from '../../../../../../core/services/asset.service';
import { LF2Spritesheet } from '../../models/lf2-spritesheet.model';
import { OBJECTS_START_POSITION_Z } from '../../../../../../shared/constants/little-fighter.constants';
import { RecordingService } from '../../../../../../core/services/recording.service';
import { SceneObjectWithFrame } from '../../models/scene-object-with-frame.model';
import { isObjectVisible } from '../../../../../../shared/utilities/lf.utility';

export default class ObjectManager extends Manager<SceneObject[], Lf2Object> {
  public constructor(
    recording: RecordingFormat,
    private readonly _assetService: AssetService,
    private readonly _recordingService: RecordingService,
  ) {
    super();

    for (const data of recording.dataList) {
      const spriteSheets: LF2Spritesheet[] = [];

      let picIndexStart = 0;
      for (const sheet of data.spritesheets) {
        const texture = this._assetService.getTexture(
          `/assets/lf2/${sheet.path}.png`,
        );
        if (!texture) {
          console.error('Object texture is undefined!');
          continue;
        }

        if (sheet.overridePicStartIndex)
          picIndexStart = sheet.overridePicStartIndex;

        spriteSheets.push({
          texture,
          spriteWidth: sheet.width,
          spriteHeight: sheet.height,
          spriteCol: sheet.col,
          spriteRow: sheet.row,
          picIndexStart,
        } as LF2Spritesheet);

        picIndexStart += sheet.col * sheet.row;
      }

      const renderCount =
        recording.maximalAmountOfObjectsPerID[data.dataID] ?? 1;
      spriteSheets.forEach((o) =>
        this.objects.push(new Lf2Object(o, data.dataID, renderCount)),
      );
    }

    this.group.position.set(0, 0, OBJECTS_START_POSITION_Z);
    this.initGroupFromObjects();
  }

  public update(data2: SceneObject[]): void {
    const visibleObjects = data2
      .map(
        (d) =>
          ({
            ...d,
            frame: this._recordingService.getFrame(d.dataID, d.frameID),
          } as SceneObjectWithFrame),
      )
      .filter((o) => isObjectVisible(o) && o.frame);

    this.objects.forEach((o) =>
      o.update(visibleObjects.filter((d) => o.shouldHandleSceneObject(d))),
    );
  }
}
