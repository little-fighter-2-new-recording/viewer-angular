import { ColorRepresentation, MeshStandardMaterial } from 'three';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import Manager from '../manager';
import SceneObject from '../../../../../../core-lib/models/scene-object.model';
import ThreeDText from '../3d-text/3d-text';
import SimpleText from '../3d-text/simple-text';
import ThreeDTextManager from '../3d-text/3d-text-manager';
import {
  OBJECTS_NAME_POSITION_OFFSET_Z,
  OBJECTS_START_POSITION_Z,
} from '../../../../../../shared/constants/little-fighter.constants';
import {
  RESERVE_POSITION_OFFSET_X,
  RESERVE_POSITION_OFFSET_Y,
} from './constants';
import { RecordingService } from '../../../../../../core/services/recording.service';
import { SceneObjectWithFrame } from '../../models/scene-object-with-frame.model';

export default class ReserveManager extends Manager<
  SceneObject[],
  ThreeDText | SimpleText,
  TextGeometry,
  MeshStandardMaterial
> {
  private readonly _backFont: Font;
  private readonly _foreFont: Font;
  private readonly _backColor: ColorRepresentation;
  private readonly _foreColor: ColorRepresentation;

  private readonly _objectToReserve: { [key: number]: ThreeDTextManager } = {};

  public constructor(
    private readonly _recordingService: RecordingService,
    backFont: Font,
    foreFont: Font,
    backColor: ColorRepresentation,
    foreColor: ColorRepresentation,
  ) {
    super();

    this._backFont = backFont;
    this._foreFont = foreFont;
    this._backColor = backColor;
    this._foreColor = foreColor;

    this.group.position.set(0, 0, OBJECTS_START_POSITION_Z);
  }

  public update(data2: SceneObject[]): void {
    const data = data2
      .map(
        (d) =>
          ({
            ...d,
            frame: this._recordingService.getFrame(d.dataID, d.frameID),
          } as SceneObjectWithFrame),
      )
      .filter((d) => d.reserve > 1 && d.frame);

    this.deleteRemovedSceneObjects(data);

    for (const sceneObject of data) {
      let manager = this._objectToReserve[sceneObject.objectID];
      const text = `x${sceneObject.reserve}`;
      if (!manager) {
        manager = new ThreeDTextManager(
          text,
          this._backFont,
          this._foreFont,
          this._backColor,
          this._foreColor,
          false,
          1,
        );
        this._objectToReserve[sceneObject.objectID] = manager;
        this.group.add(manager.getGroup());
      } else {
        manager.update(text);
      }

      manager.updatePositionAnchorCenter(
        sceneObject.x + RESERVE_POSITION_OFFSET_X,
        -sceneObject.z -
          sceneObject.y +
          sceneObject.frame.centerY +
          RESERVE_POSITION_OFFSET_Y,
      );
      manager
        .getGroup()
        .position.setZ(sceneObject.z + OBJECTS_NAME_POSITION_OFFSET_Z);
    }
  }

  private deleteRemovedSceneObjects(objects: SceneObject[]): void {
    const removedSceneIndices = Object.keys(this._objectToReserve)
      .map((i) => parseInt(i))
      .map((o) => (!objects.find((sO) => sO.objectID === o) ? o : -1))
      .filter((o) => o >= 0)
      .sort((o1, o2) => o2 - o1);

    while (removedSceneIndices.length > 0) {
      const sceneObjectIndex = removedSceneIndices.splice(0, 1)[0];
      this.group.remove(this._objectToReserve[sceneObjectIndex].getGroup());
      delete this._objectToReserve[sceneObjectIndex];
    }
  }
}
