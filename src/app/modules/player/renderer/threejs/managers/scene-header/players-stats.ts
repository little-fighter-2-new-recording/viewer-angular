import ThreeDTextManager from '../3d-text/3d-text-manager';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import { ColorRepresentation, Group } from 'three';
import SceneCharacter from '../../../../../../core-lib/models/scene-character.model';

export default class PlayersStats {
  private readonly _group = new Group();

  private readonly _backFont: Font;
  private readonly _foreFont: Font;

  private readonly _manCounter: ThreeDTextManager;
  private readonly _hpCounter: ThreeDTextManager;

  public get group(): Group {
    return this._group;
  }

  public constructor(
    backFont: Font,
    foreFont: Font,
    color: ColorRepresentation,
  ) {
    this._backFont = backFont;
    this._foreFont = foreFont;

    this._manCounter = new ThreeDTextManager(
      'Man: 0',
      this._backFont,
      this._foreFont,
      '#000000',
      color,
    );

    this._hpCounter = new ThreeDTextManager(
      'HP: 0',
      this._backFont,
      this._foreFont,
      '#000000',
      color,
    );
    this._hpCounter.getGroup().position.setX(70);

    this._group.add(this._manCounter.getGroup(), this._hpCounter.getGroup());
  }

  public update(charactersInTeam: SceneCharacter[]): void {
    const aliveCharacters = charactersInTeam.filter((c) => c.hp > 0);
    const hp =
      aliveCharacters.length <= 0
        ? 0
        : aliveCharacters.map((o) => o.hp).reduce((o1, o2) => o1 + o2);

    this._manCounter.update(`Man: ${aliveCharacters.length}`);
    this._hpCounter.update(`HP: ${hp}`);
  }
}
