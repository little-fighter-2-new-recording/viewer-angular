import { WINDOW_WIDTH } from '../../../../../../shared/constants/little-fighter.constants';

export const TEAM_BLUE_FORE_COLOR = '#7878FF';
export const INFO_FORE_COLOR = '#C8C8C8';
export const NOT_TEAM_BLUE_FORE_COLOR = '#FF00FF';

export const TEAM_BLUE_PLAYER_STATS_POSITION_X = 10;
export const TEAM_BLUE_PLAYER_STATS_RESERVE_POSITION_X = 170;
export const NOT_TEAM_BLUE_PLAYER_STATS_POSITION_X = WINDOW_WIDTH - 160;

export const INFO_POSITION_X = WINDOW_WIDTH / 2;
export const INFO_POSITION_Y = -5;
