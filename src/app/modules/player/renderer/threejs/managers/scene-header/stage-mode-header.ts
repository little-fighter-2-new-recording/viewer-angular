import { MeshStandardMaterial } from 'three';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import Manager from '../manager';
import Scene from '../../../../../../core-lib/models/scene.model';
import ThreeDText from '../3d-text/3d-text';
import SimpleText from '../3d-text/simple-text';
import ThreeDTextManager from '../3d-text/3d-text-manager';
import { getStageDescription } from '../../../../../../shared/utilities/lf.utility';
import SceneCharacter from '../../../../../../core-lib/models/scene-character.model';
import PlayersStats from './players-stats';
import {
  INFO_FORE_COLOR,
  INFO_POSITION_X,
  INFO_POSITION_Y,
  NOT_TEAM_BLUE_FORE_COLOR,
  NOT_TEAM_BLUE_PLAYER_STATS_POSITION_X,
  TEAM_BLUE_FORE_COLOR,
  TEAM_BLUE_PLAYER_STATS_POSITION_X,
  TEAM_BLUE_PLAYER_STATS_RESERVE_POSITION_X,
} from './constants';

export default class StageModeHeader extends Manager<
  Scene,
  ThreeDText | SimpleText,
  TextGeometry,
  MeshStandardMaterial
> {
  private readonly _backFont: Font;
  private readonly _foreFont: Font;

  private readonly _blueTeamStats: PlayersStats;
  private readonly _notBlueTeamStats: PlayersStats;
  private readonly _reserveCounterLeft: ThreeDTextManager;
  private readonly _centerInfo: ThreeDTextManager;

  public constructor(backFont: Font, foreFont: Font) {
    super();

    this._backFont = backFont;
    this._foreFont = foreFont;

    this._blueTeamStats = new PlayersStats(
      backFont,
      foreFont,
      TEAM_BLUE_FORE_COLOR,
    );
    this._blueTeamStats.group.position.setX(TEAM_BLUE_PLAYER_STATS_POSITION_X);

    this._reserveCounterLeft = new ThreeDTextManager(
      '',
      backFont,
      foreFont,
      '#000000',
      TEAM_BLUE_FORE_COLOR,
    );
    this._reserveCounterLeft
      .getGroup()
      .position.setX(TEAM_BLUE_PLAYER_STATS_RESERVE_POSITION_X);

    this._notBlueTeamStats = new PlayersStats(
      backFont,
      foreFont,
      NOT_TEAM_BLUE_FORE_COLOR,
    );
    this._notBlueTeamStats.group.position.setX(
      NOT_TEAM_BLUE_PLAYER_STATS_POSITION_X,
    );

    this._centerInfo = new ThreeDTextManager(
      '',
      this._backFont,
      this._foreFont,
      '#000000',
      INFO_FORE_COLOR,
    );

    const grps = [
      this._blueTeamStats.group,
      this._reserveCounterLeft.getGroup(),
      this._notBlueTeamStats.group,
      this._centerInfo.getGroup(),
    ];
    this.group.add(...grps);
  }

  public update(data: Scene): void {
    this._centerInfo.update(getStageDescription(data) ?? '');
    this._centerInfo.updatePositionAnchorCenter(
      INFO_POSITION_X,
      INFO_POSITION_Y,
    );

    const blueTeam = <SceneCharacter[]>(
      data.objects.filter((o) => o instanceof SceneCharacter && o.team === 1)
    );
    const notBlueTeam = <SceneCharacter[]>(
      data.objects.filter(
        (o) => o instanceof SceneCharacter && !blueTeam.includes(o),
      )
    );
    this._blueTeamStats.update(blueTeam);
    this._notBlueTeamStats.update(notBlueTeam);

    const reserves = blueTeam.map((o) => o.reserve - 1).filter((o) => o > 0);
    const reserve =
      reserves.length <= 0 ? 0 : reserves.reduce((r1, r2) => r1 + r2);
    this._reserveCounterLeft.update(`Reserve: ${reserve}`);
    this._reserveCounterLeft.getGroup().visible = reserve > 0;
  }
}
