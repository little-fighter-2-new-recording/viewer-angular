import {
  BufferGeometry,
  DoubleSide,
  InstancedBufferAttribute,
  PlaneBufferGeometry,
  ShaderMaterial,
  Vector2,
} from 'three';
import { Color } from 'three/src/math/Color';
import { ALPHA_TEST } from './constants';
import { SpritesheetData } from './models/spritesheet-data.model';
import ThreeJsInstancedImageObject from './three-js-instanced-image-object';
import { SPRITESHEET_VERTEX_SHADER } from './shaders/spritesheet.vertex';
import { FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY } from './shaders/spritesheet-chroma-key.fragment';

export default abstract class ThreeJsSpriteSheetObject extends ThreeJsInstancedImageObject {
  protected readonly spriteWidth: number;
  protected readonly spriteHeight: number;
  protected readonly spriteRow: number;
  protected readonly spriteCol: number;

  protected positions: InstancedBufferAttribute | undefined;
  protected uvs: InstancedBufferAttribute | undefined;
  protected mirror: InstancedBufferAttribute | undefined;

  protected constructor(
    spritesheetData: SpritesheetData,
    maxRenderCount: number,
  ) {
    super(
      spritesheetData.texture,
      maxRenderCount,
      (spritesheetData.spriteWidth + 1) * spritesheetData.spriteRow,
      (spritesheetData.spriteHeight + 1) * spritesheetData.spriteCol,
    );

    this.spriteWidth = spritesheetData.spriteWidth;
    this.spriteHeight = spritesheetData.spriteHeight;
    this.spriteRow = spritesheetData.spriteRow;
    this.spriteCol = spritesheetData.spriteCol;

    this.textureWidthHalf = this.spriteWidth / 2;
    this.textureHeightHalf = this.spriteHeight / 2;
  }

  protected generateGeometry(): BufferGeometry {
    const geometry = new PlaneBufferGeometry(
      this.spriteWidth,
      this.spriteHeight,
    );

    const attributes = this.generateAttributes();
    for (const key of Object.keys(attributes)) {
      geometry.setAttribute(key, attributes[key]);
    }

    return geometry;
  }

  protected generateAttributes(): { [key: string]: InstancedBufferAttribute } {
    this.uvs = new InstancedBufferAttribute(
      new Float32Array(2 * this.maxRenderCount),
      2,
    );
    this.mirror = new InstancedBufferAttribute(
      new Float32Array(this.maxRenderCount),
      1,
    );

    return {
      ...super.generateAttributes(),
      instanceUv: this.uvs,
      mirror: this.mirror,
    };
  }

  protected generateMaterial(): ShaderMaterial {
    return new ShaderMaterial({
      uniforms: {
        texture1: { value: this.texture },
        spritesheetSize: {
          value: new Vector2(this.textureWidth, this.textureHeight),
        },
        spriteSize: { value: new Vector2(this.spriteWidth, this.spriteHeight) },
        chromaKey: { value: new Color('#000000') },
        alphaTest: { value: ALPHA_TEST }, // see https://github.com/mrdoob/three.js/issues/15654
      },
      alphaTest: ALPHA_TEST,
      vertexShader: SPRITESHEET_VERTEX_SHADER,
      fragmentShader: FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY,
      side: DoubleSide,
    });
  }

  protected updateBufferAttributes() {
    super.updateBufferAttributes();

    if (this.uvs) this.uvs.needsUpdate = true;
    if (this.mirror) this.mirror.needsUpdate = true;
  }
}
