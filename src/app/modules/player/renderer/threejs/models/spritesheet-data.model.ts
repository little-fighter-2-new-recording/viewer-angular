import { Texture } from 'three';

export type SpritesheetData = {
  texture: Texture;
  spriteWidth: number;
  spriteHeight: number;
  spriteRow: number;
  spriteCol: number;
};
