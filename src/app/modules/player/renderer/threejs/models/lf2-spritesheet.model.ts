import { SpritesheetData } from './spritesheet-data.model';

export type LF2Spritesheet = SpritesheetData & {
  picIndexStart: number;
};
