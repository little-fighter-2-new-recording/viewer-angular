import SceneObject from '../../../../../core-lib/models/scene-object.model';
import { IDataListFrame } from '../../../../../core-lib/models/data-list-frame.interface';

export type SceneObjectWithFrame = SceneObject & {
  frame: IDataListFrame;
  shake?: number;
};
