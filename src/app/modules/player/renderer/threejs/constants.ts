/**
 * Specifies the offset by which the axes should be shifted, otherwise ThreeJS
 * will draw two pixels wide due to rounding. The problem comes from the fact
 * that ThreeJS considers the center of the box as 0,0.
 */
export const THREE_JS_OFFSET_POINT = 0.5;

export const LITTLE_FIGHTER_FONT_SIZE = 11.5; // wtf why three.js???
export const LITTLE_FIGHTER_FONT_SIZE_HEIGHT = 1; // depth
export const LITTLE_FIGHTER_FONT_CURVE_SEGMENTS = 5; // ... idk
export const LITTLE_FIGHTER_FONT_NEW_LINE_SPACING = 16;

export const ALPHA_TEST = 1 / 255;
