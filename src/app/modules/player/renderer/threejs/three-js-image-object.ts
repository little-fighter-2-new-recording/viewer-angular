import {
  DoubleSide,
  Mesh,
  PlaneGeometry,
  ShaderMaterial,
  Texture,
} from 'three';
import { Color } from 'three/src/math/Color';
import ThreeJsObject from './three-js-object';
import { ALPHA_TEST } from './constants';
import { ThreeDPosition } from '../../../../shared/models/three-d-position.model';
import { VERTEX_SHADER_IMAGE_CHROMA_KEY } from './shaders/image-chroma-key.vertex';
import { FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY } from './shaders/spritesheet-chroma-key.fragment';

export default class ThreeJsImageObject extends ThreeJsObject<
  PlaneGeometry,
  ShaderMaterial
> {
  public readonly geometry: PlaneGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: Mesh;

  protected readonly texture: Texture;
  protected readonly textureWidth: number;
  protected readonly textureHeight: number;

  private readonly _textureWidthHalf: number;
  private readonly _textureHeightHalf: number;

  public constructor(texture: Texture, width?: number, height?: number) {
    super();

    if (!texture.image && !width) throw new Error('Invalid sprite width!');
    if (!texture.image && !height) throw new Error('Invalid sprite height!');

    this.texture = texture;
    this.textureWidth = this.texture.image ? this.texture.image.width : width;
    this.textureHeight = this.texture.image
      ? this.texture.image.height
      : height;

    this._textureWidthHalf = this.textureWidth / 2;
    this._textureHeightHalf = this.textureHeight / 2;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  protected generateGeometry(): PlaneGeometry {
    return new PlaneGeometry(this.textureWidth, this.textureHeight);
  }

  protected generateMaterial(): ShaderMaterial {
    return new ShaderMaterial({
      uniforms: {
        texture1: { value: this.texture },
        chromaKey: { value: new Color(0, 0, 0) },
        alphaTest: { value: ALPHA_TEST },
      },
      vertexShader: VERTEX_SHADER_IMAGE_CHROMA_KEY,
      fragmentShader: FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY,
      side: DoubleSide,
    });
  }

  protected generateMesh(): Mesh {
    return new Mesh(this.geometry, this.material);
  }

  protected setPositionLeftTop(pos: ThreeDPosition) {
    this.mesh.position.set(
      pos.x + this._textureWidthHalf,
      pos.y - this._textureHeightHalf,
      pos.z,
    );
  }
}
