export interface IAnchorSetter {
  updatePosAnchorRightBottom(
    width: number,
    height: number,
    posX: number,
    posY: number,
  ): void;
}
