import { BufferGeometry, Material, Mesh, ShaderMaterial } from 'three';
import { Geometry } from 'three/examples/jsm/deprecated/Geometry';
import { MeshBasicMaterial } from 'three/src/materials/MeshBasicMaterial';
import { Object3D } from 'three/src/core/Object3D';
import { RGBAColor } from '../../../../shared/models/rgba-color.model';
import { rgbToHex } from '../../../../shared/utilities/color.utility';

export default abstract class ThreeJsObject<
  TGeometry extends Geometry | BufferGeometry = BufferGeometry,
  TMaterial extends Material = ShaderMaterial,
  TMesh extends Mesh | Object3D = Mesh,
> {
  public abstract readonly geometry: TGeometry;
  public abstract readonly material: TMaterial;
  public abstract readonly mesh: TMesh;

  private static isMeshBasicMaterial(
    material: any,
  ): material is MeshBasicMaterial {
    return material['color'];
  }

  public changeColor(color: RGBAColor): void {
    if (!ThreeJsObject.isMeshBasicMaterial(this.material))
      throw new Error('Invalid material!');

    this.material.color.set(rgbToHex(color));
    this.material.opacity = color.a;
    this.material.needsUpdate = true;
  }

  protected readonly renderOrder: number = 0;

  protected abstract generateGeometry(): TGeometry;
  protected abstract generateMaterial(): TMaterial;
  protected abstract generateMesh(): TMesh;
}
