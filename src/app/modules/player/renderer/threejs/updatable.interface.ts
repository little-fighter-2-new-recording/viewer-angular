export interface IUpdatable<TInput> {
  update(data: TInput): void;
}
