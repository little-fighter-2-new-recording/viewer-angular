import {
  BufferGeometry,
  InstancedMesh,
  NearestFilter,
  ShaderMaterial,
} from 'three';
import ThreeJsSpriteSheetObject from './three-js-sprite-sheet-object';
import { LF2Spritesheet } from './models/lf2-spritesheet.model';

export default class ThreeJsLF2SpriteSheetObject extends ThreeJsSpriteSheetObject {
  public readonly geometry: BufferGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh;

  public readonly spriteSheetStart: number;
  public readonly spriteSheetEnd: number;

  public constructor(data: LF2Spritesheet, maxRenderCount: number) {
    super(data, maxRenderCount);

    this.spriteSheetStart = data.picIndexStart;
    this.spriteSheetEnd =
      data.picIndexStart + this.spriteCol * this.spriteRow - 1; // -1 coz zero-based

    this.texture.repeat.set(1 / this.spriteRow, 1 / this.spriteCol);
    this.texture.minFilter = NearestFilter;
    this.texture.magFilter = NearestFilter;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial();
    this.mesh = this.generateMesh();
  }

  protected setSprite(
    index: number,
    spriteNumber: number,
    facing: boolean,
  ): void {
    const coord = this.getPicIDOffset(spriteNumber - this.spriteSheetStart);
    this.mirror?.setX(index, facing ? 1.0 : 0.0);
    this.uvs?.setXY(index, coord.x, coord.y);
  }

  protected getPicIDOffset(picID: number): { x: number; y: number } {
    if (picID > this.spriteCol * this.spriteRow) {
      console.error('Pic number is too high!');
      return { x: 0, y: 0 };
    }

    let y = 0;
    while (picID >= this.spriteRow) {
      picID -= this.spriteRow;
      ++y;
    }
    const x = picID;

    return {
      x: x,
      y: y,
    };
  }
}
