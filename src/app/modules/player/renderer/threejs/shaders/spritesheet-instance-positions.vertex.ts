export const VERTEX_SHADER_SPRITESHEET_INSTANCE_POSITIONS = `
precision highp float;

attribute vec3 instancePosition;

varying vec2 vUv;

void main() {
  vUv = uv;
  vec3 pos = position + instancePosition;
	gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );
}`;
