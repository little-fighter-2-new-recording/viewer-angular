export const FRAGMENT_SHADER_SPRITESHEET_CHROMA_KEY = `
#define EPS_RGB_DIFF 0.002
precision highp float;

uniform sampler2D texture1;
uniform float alphaTest;
uniform vec3 chromaKey;

varying vec2 vUv;

void main() {
	vec4 texelColor = texture2D( texture1, vUv );
  if ( texelColor.a < alphaTest || ( abs(texelColor.r - chromaKey.r) < EPS_RGB_DIFF && abs(texelColor.g - chromaKey.g) < EPS_RGB_DIFF && abs(texelColor.b - chromaKey.b) < EPS_RGB_DIFF ) )
    discard;

	gl_FragColor = texelColor;
}`;
