export const SPRITESHEET_VERTEX_SHADER = `
precision highp float;

uniform vec2 spritesheetSize;
uniform vec2 spriteSize;

attribute vec3 instancePosition;
attribute vec2 instanceUv;
attribute float mirror;

varying vec2 vUv;

void main() {
  vec2 spriteSizeGridLine = spriteSize + vec2(1.0, 1.0);
  vec2 spriteCoord = vec2(
    instanceUv.x * spriteSizeGridLine.x,
    spritesheetSize.y - ((instanceUv.y + 1.0) * spriteSizeGridLine.y) + 1.0 // coz reverse coordinate system (0.0 is left bottom) // +1 coz grid line
  );

  if(mirror != 0.0)
    spriteCoord.x = spritesheetSize.x - spriteCoord.x - spriteSizeGridLine.x + 1.0; // +1 coz grid line

  vUv = uv * spriteSize / spritesheetSize + (spriteCoord / spritesheetSize);

  if(mirror != 0.0)
	  vUv.x = 1.0 - vUv.x;

  vec3 pos = position + instancePosition;
	gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );
}`;
