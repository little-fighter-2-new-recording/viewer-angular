import { InstancedMesh, Material, Object3D } from 'three';
import { BufferGeometry } from 'three/src/core/BufferGeometry';
import ThreeJsObject from './three-js-object';

export default abstract class ThreeJsInstancedObject<
  TGeometry extends BufferGeometry,
  TMaterial extends Material,
> extends ThreeJsObject<TGeometry, TMaterial, InstancedMesh> {
  protected readonly maxRenderCount: number;
  protected readonly tmpObject = new Object3D();

  protected constructor(maxRenderCount: number) {
    super();

    this.maxRenderCount = maxRenderCount;
  }

  protected generateMesh(): InstancedMesh {
    const mesh = new InstancedMesh(
      this.geometry,
      this.material,
      this.maxRenderCount,
    );
    mesh.renderOrder = this.renderOrder;
    mesh.count = 0;
    return mesh;
  }
}
