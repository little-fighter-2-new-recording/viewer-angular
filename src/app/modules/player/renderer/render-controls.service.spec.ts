import { TestBed } from '@angular/core/testing';

import { RenderControlsService } from './render-controls.service';

describe('RenderControlsService', () => {
  let service: RenderControlsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RenderControlsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
