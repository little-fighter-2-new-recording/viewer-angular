export type NameColor = {
  border: string | number;
  color: string | number;
};
