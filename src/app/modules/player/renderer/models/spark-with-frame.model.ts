import Spark from '../../../../core-lib/models/spark.model';

export type SparkWithFrame = {
  spark: Spark;
  currentFrame: number;
};
