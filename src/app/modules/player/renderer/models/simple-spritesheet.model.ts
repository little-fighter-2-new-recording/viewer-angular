import { Texture } from 'three';

export type SimpleSpritesheet = {
  width: number;
  height: number;
  row: number;
  col: number;
  texture: Texture;
};
