import { SimpleSpritesheet } from './simple-spritesheet.model';

export type Spritesheet = SimpleSpritesheet & {
  picIndexStart: number;
};
