import { NameColor } from './name-color.type';

export const NAME_COLORS: { [key: number]: NameColor } = {
  0x00: {
    border: 0x444444,
    color: 0xffffff,
  },
  0x01: {
    border: 0x001e46,
    color: 0x4f9bff,
  },
  0x02: {
    border: 0x460000,
    color: 0xff4f4f,
  },
  0x03: {
    border: 0x154103,
    color: 0x3cad0f,
  },
  0x04: {
    border: 0x9a5700,
    color: 0xffd34c,
  },
  0x05: {
    border: 0x8c009a,
    color: 0xff4cec,
  },
};
