import { Texture } from 'three';

export type SmallImage = {
  texture: Texture;
  dataID: number;
  width: number;
  height: number;
};
