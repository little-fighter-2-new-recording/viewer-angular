import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import {
  Camera,
  CameraHelper,
  Clock,
  GridHelper,
  Group,
  Scene,
  WebGLRenderer,
} from 'three';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls';
import Stats from 'three/examples/jsm/libs/stats.module';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import { RendererModule } from './renderer.module';
import { environment } from '../../../../environments/environment';
import {
  changeCameraPositionX,
  changeCameraPositionY,
  createCameraAndControls,
  createCameraAndControlsDebug,
  createLights,
  createRenderer,
} from './renderer.utility';
import {
  BACKGROUND_INDEX_TO_ID,
  BACKGROUNDS,
} from '../../../core-lib/predefined/backgrounds';
import { DebugService } from '../../../core/services/debug.service';
import { RecordingFormat } from '../../../core-lib/models/recording-format.model';
import * as LF2Constants from '../../../shared/constants/little-fighter.constants';
import { AssetService as AssetService2 } from '../../../core/services/asset.service';
import { RenderControlsService } from './render-controls.service';
import { SubscribeEvents } from '../../../shared/classes/subscribe-events';
import {
  getDifficultyString,
  getModeString,
  getTotalTimeFromFPS,
} from '../../../shared/utilities/lf.utility';
import {
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../../shared/constants/little-fighter.constants';
import { CharacterBarsService } from '../../../core/services/character-bars.service';
import { RecordingService } from '../../../core/services/recording.service';
import BarsManager from './threejs/managers/bars/bars-manager';
import SparkManager from './threejs/managers/spark/spark-manager';
import ThreeDTextManager from './threejs/managers/3d-text/3d-text-manager';
import ShadowManager from './threejs/managers/shadow/shadow-manager';
import NameManager from './threejs/managers/name/name-manager';
import ReserveManager from './threejs/managers/reserve/reserve-manager';
import RecordingScene from '../../../core-lib/models/scene.model';
import ObjectManager from './threejs/managers/object/object-manager';
import BackgroundManager from './threejs/managers/background/background-manager';
import BloodManager from './threejs/managers/blood/blood-manager';
import StageModeHeader from './threejs/managers/scene-header/stage-mode-header';
import { EMode } from '../../../core-lib/models/mode.enum';

const ONE_SECOND_IN_MIL_SECOND = 1000;

@Injectable({
  providedIn: RendererModule,
})
export class RendererService extends SubscribeEvents {
  public readonly sceneCreated = new Subject<never>();

  private _canvas: HTMLCanvasElement | undefined;
  private _renderer: WebGLRenderer | undefined;
  private _scene: Scene | undefined;

  private _camera: Camera | undefined;
  private _controls: OrbitControls | undefined;

  private _cameraDebug: Camera | undefined;
  private _controlsDebug: TrackballControls | undefined;
  private _cameraHelper: CameraHelper | undefined;

  private _frameId: number | null = null;
  private _stats: Stats | undefined;
  private _clock = new Clock();
  private _clockAsyncFrames = new Clock();

  private _displayDurationMS = 0; // how long the image already displayed
  private _displayDurationMSAsync = 0; // how long the image already displayed
  private _drawCounter = 0;

  private _recording: RecordingFormat | undefined;
  private _currentGameScene: RecordingScene | undefined;
  private _blockUpdating = false;
  private _fixedGroup = new Group();
  private _mainGroup = new Group();

  private _barsManager: BarsManager | undefined;
  private _sparkManager: SparkManager | undefined;
  private _timeTextManager: ThreeDTextManager | undefined;
  private _sceneHeader: StageModeHeader | undefined;
  private _shadowManager: ShadowManager | undefined;
  private _nameManager: NameManager | undefined;
  private _reserveManager: ReserveManager | undefined;
  private _objectManager: ObjectManager | undefined;
  private _backgroundManager: BackgroundManager | undefined;
  private _bloodManager: BloodManager | undefined;

  public constructor(
    private readonly _ngZone: NgZone,
    private readonly _renderControlsService: RenderControlsService,
    private readonly _assetService2: AssetService2,
    private readonly _recordingService: RecordingService,
    private readonly _debugService: DebugService,
    private readonly _characterBars: CharacterBarsService,
  ) {
    super();
    this._clock.stop();

    this.subscribes.push(
      this._renderControlsService.currentSceneIndexEvent.subscribe(
        this.handleSceneIndexChanged.bind(this),
      ),
      this._debugService.threeDCameraEvent.subscribe(
        this.handleDebugView.bind(this),
      ),
    );
  }

  public createScene(
    recording: RecordingFormat,
    canvas: HTMLCanvasElement,
    width: number,
    height: number,
  ): void {
    this._recording = recording;

    if (!environment.production) this.createStats();

    this._canvas = canvas;
    this._renderer = createRenderer(canvas, width, height);
    this._scene = new Scene();

    const { camera, controls } = createCameraAndControls(canvas, width, height);
    this._camera = camera;
    this._controls = controls;
    this._cameraHelper = new CameraHelper(camera);
    this._cameraHelper.visible = false;

    const {
      camera: cameraDebug,
      controls: controlsDebug,
    } = createCameraAndControlsDebug(canvas, width, height);
    this._cameraDebug = cameraDebug;
    this._controlsDebug = controlsDebug;

    const lights = createLights();
    this._scene.add(...lights);

    if (!environment.production) {
      const gridHelper = new GridHelper(10000, 1000);
      gridHelper.position.y = -(442 + 128);
      this._scene.add(gridHelper);
    }

    this._mainGroup.position.set(-WINDOW_WIDTH / 2, 0, 0);

    this._scene.add(
      this._mainGroup,
      this._fixedGroup,
      this._camera,
      this._cameraHelper,
      this._cameraDebug,
    );

    this.createLFScene();
    this._clock.start();
  }

  public animate(): void {
    this._ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }
    });
  }

  public destroy(): void {
    if (this._frameId) cancelAnimationFrame(this._frameId);

    this.destroyLFScene();

    if (this._renderer) {
      this._renderer.dispose();
      this._renderer = undefined;
    }

    if (this._scene) {
      this._scene.clear();
      this._scene = undefined; // to prevent double drawing after close and reopen the tab (the service still exists as instance)
    }

    if (this._controls) {
      this._controls.dispose();
      this._controls = undefined;
    }

    if (this._controlsDebug) {
      this._controlsDebug.dispose();
      this._controlsDebug = undefined;
    }

    this._clock.stop();
    this._canvas = undefined;
    this._camera = undefined;
    this._cameraDebug = undefined;
  }

  private createStats(): void {
    this._stats = Stats();
    this._stats.showPanel(0);
    const doc = document.getElementById('stats');
    doc?.appendChild(this._stats.dom);
  }

  private render(): void {
    this._frameId = requestAnimationFrame(() => {
      this.render();

      this._stats?.update();
    });

    if (!this._blockUpdating) this.updateSceneIndex();
    if (this._debugService.threeDCamera) this._controlsDebug?.update();
    else this._controls?.update();

    if (!this._renderer || !this._scene || !this._camera) return;

    this.checkCamera();

    this._fixedGroup.position.set(
      this._camera.position.x - WINDOW_WIDTH / 2,
      this._camera.position.y + WINDOW_HEIGHT / 2,
      0,
    );

    this._backgroundManager?.update({
      cameraX: this._camera.position.x,
      drawCounter: this._drawCounter,
    });
    this._nameManager?.update({
      objects: this._currentGameScene?.objects ?? [],
      cameraX: this._camera?.position.x ?? 0,
    });

    if (this._debugService.threeDCamera && this._cameraDebug)
      this._renderer.render(this._scene, this._cameraDebug);
    else this._renderer.render(this._scene, this._camera);

    this.updateSceneSpark();
  }

  private updateScene(index: number): void {
    if (!this._recording) return;

    const scene = this._recording.scenes[index];
    this._currentGameScene = scene;

    this._sparkManager?.update(scene.sparks);
    this._barsManager?.update(scene.objects);
    this._timeTextManager?.update(
      `${getTotalTimeFromFPS(index)} / ${getTotalTimeFromFPS(
        this._recording.scenes.length,
      )}`,
    );
    this._sceneHeader?.update(scene);
    this._shadowManager?.update(scene.objects);
    this._objectManager?.update(scene.objects);
    this._reserveManager?.update(scene.objects);
    this._bloodManager?.update(scene.objects);
  }

  private updateSceneIndex(): void {
    const delta = this._clock.getDelta() * ONE_SECOND_IN_MIL_SECOND;
    const drawInterval =
      ONE_SECOND_IN_MIL_SECOND / this._renderControlsService.fps;

    this._displayDurationMS += delta;
    while (this._displayDurationMS > drawInterval) {
      this._displayDurationMS -= drawInterval;
      ++this._drawCounter;

      this._ngZone.run(() => {
        this._renderControlsService.update();
      });
    }
  }

  private updateSceneSpark(): void {
    const delta = this._clockAsyncFrames.getDelta() * ONE_SECOND_IN_MIL_SECOND;
    const drawInterval =
      ONE_SECOND_IN_MIL_SECOND / this._renderControlsService.fps;

    this._displayDurationMSAsync += delta;
    while (this._displayDurationMSAsync > drawInterval) {
      this._displayDurationMSAsync -= drawInterval;

      this._sparkManager?.updateFrames();

      // this._ngZone.run(() => {
      //   this._renderControlsService.update();
      // });
    }
  }

  private checkCamera(): void {
    if (!this._camera || !this._controls) return;

    const roundedPosX = Math.floor(this._camera.position.x);
    // const roundedPosY = Math.floor(this._camera.position.y);
    const minY = Math.floor(-LF2Constants.WINDOW_HEIGHT / 2);
    const maxX =
      this._backgroundManager && this._currentGameScene
        ? this._backgroundManager.getMaximumY(this._currentGameScene)
        : Number.MAX_SAFE_INTEGER;
    if (this._camera.position.x < 0) {
      changeCameraPositionX(this._camera, this._controls, 0);
    } else if (this._camera.position.x > maxX) {
      changeCameraPositionX(this._camera, this._controls, maxX);
    } else if (this._camera.position.x !== roundedPosX) {
      changeCameraPositionX(this._camera, this._controls, roundedPosX);
    }

    if (this._camera.position.y < minY) {
      changeCameraPositionY(this._camera, this._controls, minY);
    } else if (this._camera.position.y > minY) {
      changeCameraPositionY(this._camera, this._controls, minY);
    } /* else if (this._camera.position.y !== roundedPosY) {
      changeCameraPositionY(this._camera, this._controls, roundedPosY);
    } */
  }

  private handleSceneIndexChanged(index: number): void {
    if (index < 0) return;

    this.updateScene(index);
  }

  private handleDebugView(active3DCamera: boolean): void {
    if (!this._controlsDebug || !this._controls || !this._cameraHelper) return;
    this._cameraHelper.visible = active3DCamera;
    this._controlsDebug.enabled = active3DCamera;
    this._controls.enabled = !active3DCamera;
  }

  private createLFScene(): void {
    if (!this._recording) {
      console.warn('Recording is undefined!');
      return;
    }

    this.generateTexts();
    this.generateShadows(this._recording);
    this.generateBloods();

    this._barsManager = new BarsManager(
      this._assetService2,
      this._characterBars,
    );
    this._fixedGroup.add(this._barsManager.getGroup());

    this._sparkManager = new SparkManager(
      this._assetService2,
      this._recording.maximalSparks,
    );
    this._mainGroup.add(this._sparkManager.getGroup());

    this.updateScene(0);
    this._blockUpdating = false;
    this.sceneCreated.next();
  }

  private generateBloods(): void {
    if (!this._recording) return;

    this._bloodManager = new BloodManager(
      this._recordingService,
      this._recording.amountOfMaximalObjects,
    );
    this._mainGroup.add(this._bloodManager.getGroup());
  }

  private generateTexts(): void {
    const borderFont = this._assetService2.getFont('IN_GAME_BORDER');
    const foreFont = this._assetService2.getFont('IN_GAME');

    this.generateRecordingInfos(borderFont, foreFont);
    this.generateSceneObjectNames(borderFont, foreFont);
    this.generateSceneCharactersReserves(borderFont, foreFont);
    this.generateSceneStatsDescription(borderFont, foreFont);
  }

  private destroyLFScene(): void {
    this._mainGroup.clear();
    this._fixedGroup.clear();
  }

  private generateShadows(recording: RecordingFormat): void {
    if (
      recording.backgroundIndex === undefined ||
      BACKGROUND_INDEX_TO_ID[recording.backgroundIndex] === undefined
    )
      return;

    const background =
      BACKGROUNDS[BACKGROUND_INDEX_TO_ID[recording.backgroundIndex]];

    this._backgroundManager = new BackgroundManager(
      background,
      this._assetService2,
    );
    this._mainGroup.add(this._backgroundManager.getGroup());

    const shadowTexture = this._assetService2.getTexture(
      `BACKGROUND_SHADOW_${recording.backgroundIndex}`,
    )!;

    this._shadowManager = new ShadowManager(
      background,
      shadowTexture,
      recording.amountOfMaximalObjects,
    );

    this._mainGroup.add(this._shadowManager.getGroup());

    this._objectManager = new ObjectManager(
      this._recording!,
      this._assetService2,
      this._recordingService,
    );
    this._mainGroup.add(this._objectManager.getGroup());
  }

  private generateRecordingInfos(borderFont: Font, foreFont: Font): void {
    if (!this._recording) return;

    this.generateAuthorText(borderFont, foreFont, this._recording.author);
    this.generateInfoText(
      borderFont,
      foreFont,
      this._recording.info ?? 'Created by Luigi600\nFonts by RazenBrenday',
    );
    this.generateGameInfo(
      borderFont,
      foreFont,
      this._recording.mode,
      this._recording.difficulty,
    );
    this.generateTimeText(borderFont, foreFont, this._recording);
  }

  private generateAuthorText(
    borderFont: Font,
    foreFont: Font,
    author: string,
  ): void {
    const font = new ThreeDTextManager(
      'Author:',
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
      true,
    );
    font
      .getGroup()
      .position.set(
        LF2Constants.AUTHOR_X_POSITION,
        LF2Constants.AUTHOR_Y_POSITION,
        LF2Constants.GAME_INFOS_POSITION_Y,
      );

    const font2 = new ThreeDTextManager(
      author,
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
      true,
    );
    font2
      .getGroup()
      .position.set(
        LF2Constants.AUTHOR_X_POSITION_CONTENT,
        LF2Constants.AUTHOR_Y_POSITION,
        LF2Constants.GAME_INFOS_POSITION_Y,
      );

    this._fixedGroup.add(font.getGroup(), font2.getGroup());
  }

  private generateInfoText(
    borderFont: Font,
    foreFont: Font,
    info: string,
  ): void {
    const font = new ThreeDTextManager(
      'Info:',
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
      true,
    );
    font
      .getGroup()
      .position.set(
        LF2Constants.INFO_X_POSITION,
        LF2Constants.INFO_Y_POSITION,
        LF2Constants.GAME_INFOS_POSITION_Y,
      );

    const font2 = new ThreeDTextManager(
      info,
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
      true,
    );
    font2
      .getGroup()
      .position.set(
        LF2Constants.INFO_X_POSITION_CONTENT,
        LF2Constants.INFO_Y_POSITION,
        LF2Constants.GAME_INFOS_POSITION_Y,
      );

    this._fixedGroup.add(font.getGroup(), font2.getGroup());
  }

  private generateGameInfo(
    borderFont: Font,
    foreFont: Font,
    mode: number,
    difficulty: number,
  ): void {
    const font = new ThreeDTextManager(
      `${getModeString(mode)} (${getDifficultyString(difficulty)})`,
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
      true,
    );
    font.updatePosAnchorRightBottom(
      WINDOW_WIDTH,
      -WINDOW_HEIGHT,
      LF2Constants.GAME_INFO_POSITION_RIGHT_BOTTOM_OFFSET_X,
      LF2Constants.GAME_INFO_POSITION_RIGHT_BOTTOM_OFFSET_Y,
    );
    this._fixedGroup.add(font.getGroup());
  }

  private generateSceneStatsDescription(
    borderFont: Font,
    foreFont: Font,
  ): void {
    if (!this._recording) return;

    if (this._recording.mode === EMode.STAGE) {
      this._sceneHeader = new StageModeHeader(borderFont, foreFont);

      const grp = this._sceneHeader.getGroup();
      grp.position.set(
        0,
        LF2Constants.GAME_INFO_STAGE_POSITION_Y,
        LF2Constants.GAME_INFOS_POSITION_Y,
      );
      this._fixedGroup.add(grp);
    }
  }

  private generateTimeText(
    borderFont: Font,
    foreFont: Font,
    recording: RecordingFormat,
  ): void {
    this._timeTextManager = new ThreeDTextManager(
      `${getTotalTimeFromFPS(0)} / ${getTotalTimeFromFPS(
        recording.scenes.length,
      )}`,
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
      true,
      0,
    );
    this._timeTextManager
      .getGroup()
      .position.set(
        LF2Constants.TIME_X_POSITION,
        LF2Constants.TIME_Y_POSITION,
        LF2Constants.GAME_INFOS_POSITION_Y,
      );
    this._fixedGroup.add(this._timeTextManager.getGroup());
  }

  private generateSceneObjectNames(borderFont: Font, foreFont: Font): void {
    if (!this._recording) return;

    this._nameManager = new NameManager(
      this._recording.players,
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
    );
    this._mainGroup.add(this._nameManager.getGroup());
  }

  private generateSceneCharactersReserves(
    borderFont: Font,
    foreFont: Font,
  ): void {
    this._reserveManager = new ReserveManager(
      this._recordingService,
      borderFont,
      foreFont,
      LF2Constants.THREE_D_BORDER_COLOR,
      LF2Constants.THREE_D_FORE_COLOR,
    );
    this._mainGroup.add(this._reserveManager.getGroup());
  }
}
