import {
  AmbientLight,
  Camera,
  MOUSE,
  Object3D,
  OrthographicCamera,
  PerspectiveCamera,
  WebGLRenderer,
} from 'three';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { MAXIMUM_POSITION_Z } from '../../../shared/constants/little-fighter.constants';

export function createRenderer(
  canvas: HTMLCanvasElement,
  width: number,
  height: number,
): WebGLRenderer {
  const renderer = new WebGLRenderer({
    canvas: canvas,
    alpha: true, // transparent background
    antialias: false,
  });
  renderer.setSize(width, height);

  return renderer;
}

export function createCameraAndControls(
  canvas: HTMLCanvasElement,
  width: number,
  height: number,
): { camera: Camera; controls: OrbitControls } {
  const camera = new OrthographicCamera(
    width / -2,
    width / 2,
    height / 2,
    height / -2,
    0.1,
    1000000,
  );
  const controls = new OrbitControls(camera, canvas);
  controls.enabled = true;
  controls.enableRotate = false;
  controls.enableZoom = false;
  controls.panSpeed = 0.8;
  controls.mouseButtons = {
    LEFT: MOUSE.PAN,
    MIDDLE: MOUSE.PAN,
    RIGHT: MOUSE.PAN,
  };

  camera.position.set(0, 0, MAXIMUM_POSITION_Z);
  camera.lookAt(10, 0, 0);
  controls.update();

  return { camera, controls };
}

export function createCameraAndControlsDebug(
  canvas: HTMLCanvasElement,
  width: number,
  height: number,
): { camera: Camera; controls: TrackballControls } {
  const camera = new PerspectiveCamera(75, width / height, 0.1, 100000000);
  const controls = new TrackballControls(camera, canvas);
  controls.enabled = false;
  controls.rotateSpeed = 2.0;
  controls.zoomSpeed = 1.0;
  controls.panSpeed = 0.8;

  camera.position.set(0, 50, 100);
  controls.update();

  return { camera, controls };
}

export function createLights(): Object3D[] {
  // const directionLight1 = new DirectionalLight(0xffffff, 0.9);
  // directionLight1.castShadow = true;
  // const directionLight2 = new DirectionalLight(0xffffff, 0.9);
  // directionLight2.castShadow = true;

  const light = new AmbientLight(0xffffff, 1.0);
  return [
    // directionLight1,
    // directionLight1.target,
    // directionLight2,
    // directionLight2.target,
    light,
  ];
}

export function changeCameraPositionX(
  camera: Camera,
  controls: OrbitControls | TrackballControls,
  newX: number,
): void {
  camera.position.x = newX;
  controls.target.set(newX, controls.target.y, controls.target.z);
  controls.update();
}

export function changeCameraPositionY(
  camera: Camera,
  controls: OrbitControls | TrackballControls,
  newY: number,
): void {
  camera.position.y = newY;
  controls.target.set(controls.target.x, newY, controls.target.z);
  controls.update();
}
