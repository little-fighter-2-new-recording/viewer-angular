import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecordingService } from '../../../core/services/recording.service';
import { DebugService } from '../../../core/services/debug.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [RecordingService, DebugService],
})
export class RendererModule {}
