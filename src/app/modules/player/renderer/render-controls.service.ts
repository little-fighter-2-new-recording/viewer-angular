import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RendererModule } from './renderer.module';
import { RecordingService } from '../../../core/services/recording.service';

const DEFAULT_FPS = 30;

@Injectable({
  providedIn: RendererModule,
})
export class RenderControlsService {
  private readonly _pause = new BehaviorSubject<boolean>(false);
  private readonly _currentSceneIndex = new BehaviorSubject<number>(-1);
  private readonly _fps = new BehaviorSubject<number>(DEFAULT_FPS);

  public readonly pauseEvent = this._pause.asObservable();
  public readonly currentSceneIndexEvent =
    this._currentSceneIndex.asObservable();
  public readonly fpsChanged = this._fps.asObservable();

  public isLoading = false;

  public get currentSceneIndex(): number {
    return this._currentSceneIndex.value;
  }

  public set currentSceneIndex(index: number) {
    this._currentSceneIndex.next(index);
  }

  public get pause(): boolean {
    return this._pause.value;
  }

  public set pause(isPause: boolean) {
    this._pause.next(isPause);
  }

  public get fps(): number {
    return this._fps.value;
  }

  public set fps(val: number) {
    if (val < 1) return;

    this._fps.next(val);
  }

  public constructor(private readonly _recordingService: RecordingService) {}

  public update(): void {
    if (this._pause.value || this.isLoading) return;

    this.nextFrame();
  }

  public nextFrame(): void {
    if (!this._recordingService.recording) return;

    if (
      this._currentSceneIndex.value <
      this._recordingService.recording.scenes.length - 1
    )
      this._currentSceneIndex.next(this._currentSceneIndex.value + 1);
  }

  public previousFrame(): void {
    if (!this._recordingService.recording) return;

    if (this._currentSceneIndex.value > 0)
      this._currentSceneIndex.next(this._currentSceneIndex.value - 1);
    else this._currentSceneIndex.next(0);
  }

  public lastFrame(): void {
    if (!this._recordingService.recording) return;

    if (
      this._currentSceneIndex.value !==
      this._recordingService.recording.scenes.length - 1
    )
      this._currentSceneIndex.next(
        this._recordingService.recording.scenes.length - 1,
      );
  }

  public firstFrame(): void {
    if (!this._recordingService.recording) return;

    if (this._currentSceneIndex.value !== 0) this._currentSceneIndex.next(0);
  }
}
