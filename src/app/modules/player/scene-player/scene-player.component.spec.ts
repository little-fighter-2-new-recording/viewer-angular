import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenePlayerComponent } from './scene-player.component';

describe('ScenePlayerComponent', () => {
  let component: ScenePlayerComponent;
  let fixture: ComponentFixture<ScenePlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScenePlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
