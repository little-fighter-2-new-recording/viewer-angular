import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { CdkDragMove } from '@angular/cdk/drag-drop';
import { Position } from '../../../shared/models/position.model';
import {
  BAR_BACKGROUND_HEIGHT,
  BAR_BACKGROUND_WIDTH,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../../shared/constants/little-fighter.constants';
import { RendererService } from '../renderer/renderer.service';
import { RenderControlsService } from '../renderer/render-controls.service';
import { RecordingService } from '../../../core/services/recording.service';
import { CharacterBarsService } from '../../../core/services/character-bars.service';
import { SubscribeEvents } from '../../../shared/classes/subscribe-events';
import { RecordingFormat } from '../../../core-lib/models/recording-format.model';

const DOCKING_TOLERANCE = 10;

@Component({
  selector: 'nlfr-scene-player[recording]',
  templateUrl: './scene-player.component.html',
  styleUrls: ['./scene-player.component.scss'],
})
export class ScenePlayerComponent
  extends SubscribeEvents
  implements OnDestroy, OnChanges {
  public readonly characterBars: Position[];

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas!: ElementRef<HTMLCanvasElement>;

  @Input()
  public recording: RecordingFormat | undefined | null;

  constructor(
    private readonly _rendererService: RendererService,
    private readonly _renderControlsService: RenderControlsService,
    private readonly _recordingService: RecordingService,
    private readonly _characterBarsService: CharacterBarsService,
  ) {
    super();

    this.characterBars = [...this._characterBarsService.barPositions];
  }

  ngOnChanges(): void {
    this._rendererService.destroy();
    if (
      !this.rendererCanvas ||
      !this.rendererCanvas.nativeElement ||
      !this.recording
    )
      return;

    this._rendererService.createScene(
      this.recording,
      this.rendererCanvas.nativeElement,
      WINDOW_WIDTH,
      WINDOW_HEIGHT,
    );
    this._rendererService.animate();
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();

    console.log('DEST VIEWER THREEJS');
    this._rendererService.destroy();
  }

  barPositionChanged(eventArg: CdkDragMove, index: number): void {
    const parentParent = eventArg.source.element.nativeElement.parentElement;
    if (!parentParent) return;

    let { x, y } = eventArg.source.getFreeDragPosition();
    x +=
      parentParent.offsetLeft +
      eventArg.source.element.nativeElement.offsetLeft;
    y +=
      parentParent.offsetTop + eventArg.source.element.nativeElement.offsetTop;

    const dockedXY = this.getDockedXY(index, x, y);

    this._characterBarsService.changeBarPosition(index, dockedXY);
  }

  endDragPlayerBar(): void {
    this.characterBars.splice(
      0,
      this.characterBars.length,
      ...this._characterBarsService.barPositions,
    );
  }

  private getDockedXY(index: number, x: number, y: number): Position {
    const barPos = this._characterBarsService.barPositions;
    const dockableX = [WINDOW_WIDTH - BAR_BACKGROUND_WIDTH];
    const dockableY = [WINDOW_HEIGHT - BAR_BACKGROUND_HEIGHT];
    for (let i = 0; i < barPos.length; ++i) {
      if (i === index) continue;

      dockableX.push(
        barPos[i].x,
        barPos[i].x + BAR_BACKGROUND_WIDTH,
        barPos[i].x - BAR_BACKGROUND_WIDTH,
      );

      dockableY.push(
        barPos[i].y,
        barPos[i].y + BAR_BACKGROUND_HEIGHT,
        barPos[i].y - BAR_BACKGROUND_HEIGHT,
      );
    }

    const dockX = dockableX
      .map((d) => d - x)
      .sort((a1, a2) => a2 - a1)
      .find((d) => Math.abs(d) < DOCKING_TOLERANCE);
    if (dockX !== undefined) x += dockX;

    const dockY = dockableY
      .map((d) => d - y)
      .sort((a1, a2) => a2 - a1)
      .find((d) => Math.abs(d) < DOCKING_TOLERANCE);
    if (dockY !== undefined) y += dockY;

    return { x, y };
  }
}
