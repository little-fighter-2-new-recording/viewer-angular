import { Component } from '@angular/core';
import { RecordingService } from '../../../core/services/recording.service';
import { RenderControlsService } from '../renderer/render-controls.service';

@Component({
  selector: 'nlfr-player-controls',
  templateUrl: './player-controls.component.html',
  styleUrls: ['./player-controls.component.scss'],
})
export class PlayerControlsComponent {
  public maxFrameIndex = 0;

  constructor(
    public readonly recordingService: RecordingService,
    public readonly renderControlsService: RenderControlsService,
  ) {
    this.recordingService.recording$.subscribe((recording) => {
      if (recording) this.maxFrameIndex = recording.scenes.length - 1;
    });
  }

  changeCurrentIndex(event: Event): void {
    const val = (event.target as HTMLInputElement).value;
    if (val === null) return;

    try {
      this.changeCurrentIndexNumber(parseInt(val));
    } catch (e) {
      console.warn(e);
    }
  }

  changeCurrentIndexNumber(number: number | null): void {
    if (number === null) return;

    this.renderControlsService.currentSceneIndex = number;
  }

  changeFPS(event: Event): void {
    const val = (event.target as HTMLInputElement).value;
    if (val === null) return;

    try {
      this.changeFPSNumber(parseInt(val));
    } catch (e) {
      console.warn(e);
    }
  }

  changeFPSNumber(val: number | null): void {
    if (val === null) return;

    try {
      this.renderControlsService.fps = val;
    } catch (e) {
      console.warn(e);
    }
  }
}
