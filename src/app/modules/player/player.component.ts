import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RenderControlsService } from './renderer/render-controls.service';
import { RecordingService } from '../../core/services/recording.service';
import { SubscribeEvents } from '../../shared/classes/subscribe-events';
import { AssetService } from '../../core/services/asset.service';
import {
  LITTLE_FIGHTER_2_SMALL,
  SYSTEM_BAR_BACKGROUND,
  SYSTEM_SPARK,
} from '../../shared/constants/assets.constants';
import { SPARKS } from './renderer/threejs/managers/spark/sparks.constants';
import { NAMES } from '../../core-lib/predefined/names';
import {
  BACKGROUND_INDEX_TO_ID,
  BACKGROUNDS,
} from '../../core-lib/predefined/backgrounds';
import LayerImage from '../../core-lib/models/layer-image.model';
import BackgroundManager from './renderer/threejs/managers/background/background-manager';

@Component({
  selector: 'nlfr-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent extends SubscribeEvents implements OnInit {
  @ViewChild('resizer', { static: true })
  public resizer: ElementRef<HTMLDivElement> | undefined;

  constructor(
    private readonly _renderControlsService: RenderControlsService,
    public readonly recordingService: RecordingService,
    public readonly assetsService: AssetService,
  ) {
    super();

    this.subscribes.push(
      this.recordingService.recording$.subscribe((recording) => {
        if (!recording) return;

        this.assetsService.getOrLoadTexture(
          `/assets/lf2/skin/bars.png`,
          SYSTEM_BAR_BACKGROUND,
        );

        for (const spark of SPARKS) {
          this.assetsService.getOrLoadTexture(
            `/assets/lf2/skin/${spark.path}.png`,
            `${SYSTEM_SPARK}_${spark.path}`,
          );
        }

        for (const data of recording.dataList) {
          const smallImage = NAMES[data.dataID];
          if (!smallImage) break;

          this.assetsService.getOrLoadTexture(
            `/assets/lf2/sprites/${smallImage}_s.png`,
            `${LITTLE_FIGHTER_2_SMALL}_${data.dataID}`,
          );
        }

        for (const data of recording.dataList) {
          for (const sheet of data.spritesheets) {
            this.assetsService.getOrLoadTexture(
              `/assets/lf2/${sheet.path}.png`,
            );
          }
        }

        this.assetsService.getOrLoadFont(
          '/assets/fonts/RZ_InGame_Lui_Ver4.ttf',
          'IN_GAME',
        );
        this.assetsService.getOrLoadFont(
          '/assets/fonts/RZ_InGame_Border_Lui_Ver4.ttf',
          'IN_GAME_BORDER',
        );

        if (
          recording.backgroundIndex === undefined ||
          BACKGROUND_INDEX_TO_ID[recording.backgroundIndex] === undefined
        )
          return;

        const background =
          BACKGROUNDS[BACKGROUND_INDEX_TO_ID[recording.backgroundIndex]];

        this.assetsService.getOrLoadTexture(
          BackgroundManager.getShadowAssetPath(background.shadowPath),
          `BACKGROUND_SHADOW_${recording.backgroundIndex}`,
        );

        (<LayerImage[]>(
          background.layers.filter((l) => l instanceof LayerImage)
        )).forEach((l) => {
          this.assetsService.getOrLoadTexture(
            BackgroundManager.getLayerAssetPath(l.path),
          );
        });
      }),
    );
  }

  ngOnInit(): void {
    window.addEventListener('keydown', (event) => {
      if (event.key === 'F2') {
        if (!this._renderControlsService.pause)
          this._renderControlsService.pause = true;
        else this._renderControlsService.nextFrame();
      } else if (event.key === 'F1') {
        this._renderControlsService.pause = !this._renderControlsService.pause;
      }
    });
    console.log('INIT VIEWER THREEJS');
  }

  /*
  private getStageDescription(): string {
    if (
      !this._recordingService.recording ||
      this._renderControlsService.currentSceneIndex < 0
    )
      return '';
    const scene = this._recordingService.recording.scenes[
      this._renderControlsService.currentSceneIndex
    ];

    if (!(scene instanceof StageScene)) return '';

    if (scene.stage === LITTLE_FIGHTER_STAGE_SURVIVAL)
      return `Survival Stage ${scene.phase}`;

    return `Stage - `;
  }
   */
}
