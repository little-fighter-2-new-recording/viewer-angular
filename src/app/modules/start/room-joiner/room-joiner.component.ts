import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RECORDING_TITLE_MIN_LENGTH } from '../../../core-lib/dtos/constants';
import RoomJoinDTO from '../../../core-lib/dtos/room-join.dto';
import { RoomService } from '../../../core/services/room.service';
import { SubscribeEvents } from '../../../shared/classes/subscribe-events';

@Component({
  selector: 'nlfr-room-joiner',
  templateUrl: './room-joiner.component.html',
  styleUrls: ['./room-joiner.component.scss'],
})
export class RoomJoinerComponent
  extends SubscribeEvents
  implements OnInit, OnDestroy
{
  public data: Partial<RoomJoinDTO> = {
    roomID: '',
  };

  public isLoading = false;

  public readonly minLengthName = RECORDING_TITLE_MIN_LENGTH;

  constructor(
    private readonly _activatedRouted: ActivatedRoute,
    private readonly _router: Router,
    private readonly _roomService: RoomService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscribes.push(
      this._activatedRouted.paramMap.subscribe((params) => {
        const paramID = params.get('id');
        if (paramID) this.data.roomID = paramID;
      }),
    );
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  async submit(): Promise<void> {
    if ((this.data.password?.length ?? 0) <= 0) delete this.data.password;

    const result = await this._roomService.joinRoom(this.data as RoomJoinDTO);
    if (!result) return;

    const hasRecordingFile = await this._roomService.hasRecordingFile();
    if (hasRecordingFile) {
      await this._roomService.getAndSetRecordingAndRecordingSettings();
    }

    await this._router.navigate(['/viewer']);
  }
}
