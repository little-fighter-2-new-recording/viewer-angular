import { Component } from '@angular/core';
import RoomCreatorDTO from '../../../core-lib/dtos/room-creator.dto';
import { EPermission } from '../../../core-lib/dtos/permission.enum';
import { RECORDING_TITLE_MIN_LENGTH } from '../../../core-lib/dtos/constants';
import { RoomService } from '../../../core/services/room.service';
import { Router } from '@angular/router';
import { FileSelectorService } from '../../../core/services/file-selector.service';
import BinaryParser from '../../../core-lib/parsers/binary-parser';
import { RecordingService } from '../../../core/services/recording.service';
import { RenderControlsService } from '../../player/renderer/render-controls.service';

@Component({
  selector: 'nlfr-room-creator',
  templateUrl: './room-creator.component.html',
  styleUrls: ['./room-creator.component.scss'],
})
export class RoomCreatorComponent {
  public data: Partial<RoomCreatorDTO> = {
    name: '',
    defaultPermission: EPermission.equalRights,
  };

  public isLoading = false;
  public files: Blob[] = [];

  public readonly minLengthName = RECORDING_TITLE_MIN_LENGTH;

  constructor(
    private readonly _roomService: RoomService,
    private readonly _router: Router,
    private readonly _fileSelectorService: FileSelectorService,
    private readonly _recordingService: RecordingService,
    private readonly _renderControlsService: RenderControlsService,
  ) {
    this.files = this._fileSelectorService.files.value;
    this._fileSelectorService.files.subscribe((files) => (this.files = files));
  }

  openFileSelector(): void {
    this._fileSelectorService.openFileSelector.next();
  }

  async submit(): Promise<void> {
    this.isLoading = true;

    try {
      const result = await this._roomService.createRoom(
        this.data as RoomCreatorDTO,
      );
      if (!result) return;

      const file = this.files[0];
      const bytes = new Uint8Array(await file.arrayBuffer());
      this._recordingService.recording = undefined;
      this._renderControlsService.currentSceneIndex = 0;
      this._recordingService.recording = new BinaryParser(bytes).parse();

      const resultUpload = await this._roomService.testUpload(bytes);
      if (!resultUpload) return;

      await this._router.navigate(['/viewer']);
    } catch (e) {
      console.warn(e);
    } finally {
      this.isLoading = false;
    }
  }
}
