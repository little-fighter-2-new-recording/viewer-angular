import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'nlfr-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(private readonly _router: Router) {}

  test(): void {
    this._router.navigate(['/viewer']);
    console.log('TESS');
  }
}
