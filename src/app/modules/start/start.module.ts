import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RoomCreatorComponent } from './room-creator/room-creator.component';
import { RoomJoinerComponent } from './room-joiner/room-joiner.component';

@NgModule({
  declarations: [HomeComponent, RoomCreatorComponent, RoomJoinerComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
  ],
  exports: [HomeComponent, RoomCreatorComponent, RoomJoinerComponent],
})
export class StartModule {}
