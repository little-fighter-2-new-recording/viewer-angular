import { app, BrowserWindow, ipcMain } from 'electron';
import { join } from 'path';
import {
  EVENT_GET_RECORDING_BINARY,
  EVENT_GET_RECORDING_RESULT_BINARY,
} from './events.constants';
import { APP_PATH } from './constants';
import { asyncReadFile } from './async.static';

const isDebugMode = !app.isPackaged; // see: https://www.electronjs.org/docs/api/app#appispackaged-readonly   !(process.env.NODE_ENV === 'production');
const URL = isDebugMode
  ? 'http://localhost:4200/viewer'
  : join(__dirname, '../viewer/index.html');

let mainWindow: BrowserWindow;
if (isDebugMode) {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('electron-reload')(__dirname, {
    electron: require(`${__dirname}/../../node_modules/electron`),
  });
}

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    icon: join(__dirname, '../llui/assets/icon.png'),
    webPreferences: {
      nodeIntegration: true, // Allows IPC and other APIs
    },

    show: !isDebugMode,
  });
  if (URL.startsWith('http')) mainWindow.loadURL(URL);
  else mainWindow.loadFile(URL);

  if (isDebugMode) mainWindow.webContents.openDevTools();

  mainWindow.setFullScreen(!isDebugMode);
  mainWindow.setMenuBarVisibility(isDebugMode);

  if (isDebugMode) mainWindow.showInactive();
  // mainWindow.setOpacity(0.2);
});

app.on('window-all-closed', () => {
  app.quit();
});

ipcMain.on(EVENT_GET_RECORDING_BINARY, async (event, arg: string) => {
  const config = await asyncReadFile(join(APP_PATH, arg));
  event.reply(EVENT_GET_RECORDING_RESULT_BINARY, config);
});
