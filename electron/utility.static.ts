export function getExtension(path: string): string {
  const lastDot = path.lastIndexOf('.');
  if (lastDot < 0) return '';

  return path.substring(lastDot + 1).toLowerCase();
}
