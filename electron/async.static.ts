import { promisify } from 'util';
import { readdir, readFile } from 'fs';

export const asyncReadFile = promisify(readFile);
export const asyncReadDir = promisify(readdir);
